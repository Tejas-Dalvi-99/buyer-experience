## [2.1.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.0.0...v2.1.0) (2023-06-16)


### 🌍 i18n change

* Add french human translated content for software faster page ([094a08d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/094a08dc5cef5aee63746bfaedbd3b5b37a87b39))
* add translated page titles to solutions - startups page ([5e12394](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5e12394e055e217d63b626b6c35434acb54c40c3))
* **content:** German Enterprise page - Human translation review ([1082c04](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1082c04ea399edbe7bd177d87e724ffa4fef8928))
* **content:** German Platform page - Human translation review ([f5d68b3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f5d68b3b17620a4c96452047a6eb836dcc95ea02))
* **content:** German Small Business page - Human translation review ([ee9563d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ee9563dab336bed8ddeade2febe58f1cae93ccef))
* **content:** Japanese DevOps Platform Topics page - Human translation review ([f9cae43](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f9cae4370c0a454204bb57160ac8784d0a529d24))
* **content:** Japanese Enterprise page - Human translation review ([b31a8f2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b31a8f20dbb7835616c2492dfe5cf58e418dd4a3))
* **content:** Japanese Platform page - Human translation review ([a9eab20](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a9eab20fbfe53b6990ab6b0c7bf32b323c66c615))
* **content:** Japanese Small Business page - Human translation review ([1a3d2f6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1a3d2f6e61ca06f4fd6304485fb58510b921f111))
* **content:** Japanese Software Faster page - Human translation review ([5dcc362](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5dcc362fb229039b0e1ce931a6ab7d61d6356588))
* **content:** Japanese TeamOps page - Human translation review ([9bc5bd8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9bc5bd8f10d58d1ffb2fb0d405617f7a0afb77d0))
* **Enterprise:** French Enterprise page - Human translation review ([abfac1f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/abfac1f01ddf72765aa1b77223cfcef2dbd95a1d))
* Resolve "French Get Started - Enterprise page - Human translation review" ([cdcde9c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cdcde9cf76a42da73cc6478d23f84c04cac3c62c))
* Resolve "German DevOps Platform Topics page - Human translation review" ([aa83c37](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/aa83c37b0493b213cd1c596c5d103b7a9262e278))
* Resolve "German Get Started - Enterprise page - Human translation review" ([dc440b3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dc440b39a5541d22a295700c705456788be56310))
* Resolve "Japanese Get Started - Enterprise page - Human translation review" ([a81c778](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a81c778ac1b961bb7abd4a78f13114e9edb4adcb))
* Update french devops-platform topic page with human translated content ([8ee137b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8ee137b86ed7c68ffa851159ab0ea52329e38813))
* Update french platform page with human translated content ([64a79fd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/64a79fd0e932ddf105cb33fc547957fffd479b4b))
* Update french small business content with human translated content ([53a44bc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/53a44bc9ed77ba595eb09475c3e728cd7e722cb3))


### 🚀 Features

* Add code-suggestions component ([f7d984b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f7d984bcf0e22b0d38d36e1604cff92f03fd6410))
* **events:** city form closing option ([b99b754](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b99b75462ac7449bb8422e57001011f6463f7e96)), closes [#2552](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2552)
* Gartner Banner & 3.3.7 Navigation Release ([0506a57](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0506a57e777041cf6902638c85d4629b94447247)), closes [#2525](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2525)
* Navigation release 4.0.1 ([4bdfd6e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4bdfd6e83a86e338a9fec1b82e023faef73b65b1))
* New get-started/CI page ([ef2cd8c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ef2cd8ced11aa0d7522fcc2ac6818a820a1a5d43))
* Resolve "[ENG] /customers/curve to migrate to BE" ([fc16a29](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fc16a29ff864c0fc6d45c05ee34652177614007c))
* Resolve "[ENG] New Solutions page InnerSource" ([7898f61](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7898f61dde5b4859f6c85029af789a865559a0a4))
* Update dedicated page ([4ad04c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4ad04c327b9471a1686201da7661390d0630e602))
* Update topics dynamic card logic to include subdirectories and add trailing slash to paths ([2b72587](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2b725876cc89fd24fb3d3eb09e9e59d084a9c5c7))
* World tour region grouping ([8af8f6f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8af8f6f340316431d9a857680fe82a5d1b39edbb))


### ♻️ Refactors

* **meta:** Hreflang attribute for english pages ([24a3ae7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/24a3ae7e939f77c23026ef5c3c933233995b8646))
* Resolve "[ENG] Refactor InnerSource page" ([086fe09](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/086fe09ff35f5a4dc0f2d4d1fb98d46c6d1d3307))


### 🗂️ Content change

* [ENG] /topics landing page links missing trailing slash ([75f6a1d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/75f6a1d552575108614f30800db9cf427a8f97c2))
* Add Gartner DevOps Platforms report to Resources page ([d95691d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d95691d40bca62a2a1f701c59a947f04edee2784))
* Adding AWS Summit São Paulo 2023 to the events page. ([9a1a73b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9a1a73bcebc351f2fb0b36b658a9c79b4abf382d))
* Adding Brazil Making the case for CI/CD in your Organization to the events page. ([5efc6a1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5efc6a1dd1ba331089c0a4e4bb87d2e11c7a794e))
* Adding GitLab Connect East Bay to the events page. ([cfd14e4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cfd14e494f5b0348627dab231732a0bc636d9a28))
* Adding Presentando el caso de CI/CD en tu organización to the events page. ([d05e023](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d05e02370e3759e9afafaf59416db7b1b32e42ed))
* DevSecOps Survey Q2 Updates ([1173cb3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1173cb30b142352a60267c948b510c93d2a069f4))
* **events:** Radovan Bacovic added Crunch Data Conference details ([4357d8c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4357d8c8d0c6ffcccfa4aecbe0743700a40d7dbd))
* fix internal redirects on homepage ([a1d213d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a1d213d7ef9abeb263f19ffbb881377c03dfa965))
* Fix invalid links to documentation on self-managed renewal ([c7820d0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c7820d03824ef349a82c2df2f5cc63c1b41b60e3))
* **open-source:** add logo for new partner Colmena ([d5ef314](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d5ef3144ab2ba6b63b445c8abf5830d91b34860d))
* Removing code suggestions from pricing page ([0f07647](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0f07647d0ddb31d823ff037a3ed24230985bcbea))
* Removing LATAM Making the case for CI/CD in your Organization from the events page. ([32425a8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32425a82da92eddacb7fd767bd342dc15f65e451))
* **resources:** Add Gartner MQ AST report to Resources ([7ac0dbd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7ac0dbd281d766c926ea66ee6930f5480e4c9371))
* **solutions:** Add Eclipse Foundation to open source partner page ([51d7c21](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/51d7c219ff6b25f6c5b32ddd0f072d1e516ef3f4))
* udpate start time for GitLab 16 ([1dff5e6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1dff5e61d91fc2f688d47ddf3631b4386fc56068))
* Update AWS re:inforce lightning talks ([9790c8a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9790c8ad2c9695e8bf774f83c56d811fec1fec78))
* Update broken handbook hyperlinks ([b074494](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b07449421f59dcb169692f536a4c701faad68b9c))
* update meta image ([2694695](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2694695e5c880566eeddbfb27f19efc9c8bacf63))
* update meta image ([32bc3ff](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32bc3ff352d7461fe28ed82b5d7d7d9147360707))
* Updating the Chicago page's Keynote session to say Chicago and not Atlanta ([44a1fd4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/44a1fd40547e3d352c29e86b0662baa29c0cd6e3))


### 🐜 Bug Fixes

* for new ci page link ([0defadc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0defadc748be3afb3b37f36a982ea8c5e486b11e))
* gitlab infomercial video not loading on enterprise page ([8cffc41](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8cffc41f28e6f53a649ef405152afd08281545cc))
* platform page de-de and ja-jp report cta ([428177e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/428177e9caf874cba087374010e2f97cce6fc83e))

## [2.0.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.10.0...v2.0.0) (2023-06-02)


### ⚠ BREAKING CHANGES

* **Localization:** Localized Marketing Site Release

### ⚡ Performance Improvements

* Resolve "[ENG] Add lazy loading to large payload pages" ([64684dc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/64684dc485b98c229030d75be1bc22c9663ded0b))


### ♻️ Refactors

* Resolve "Remove Competitive Pages" ([f89ee11](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f89ee1166812ad903083ae5f3419ed96b43fe834))


### 🌍 i18n change

* i18n pages built that aren't actually i18n ([33817aa](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/33817aaecf064a5cfc17204f8251be13ca62d11e))
* Resolve "German Sales page - Human translation review" ([7ba4c2e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7ba4c2e5e6e46e78247ff078a811f05f6e721856))


### 🐜 Bug Fixes

* Better aligned headshots to avoid cropping in world tour city pages ([b20da97](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b20da97efbd91aa645ac41776bca6b768f9f42fd))
* **calculators:** retain campaign params when using calculator ([c61224a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c61224aec7b4703c298ab2b22f21707339056470))
* **case studies:** prevent logo svgs from rendering too small ([ea3fe5b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ea3fe5ba44e39697ee1e7105b672c70829da65f0))
* **homepage:** Fix Resources cards responsive styles in tablet ([4eeea44](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4eeea44cbd282bf216e95afc9ebe4777c8975460))
* **homepage:** Fix Resources cards responsive styles in tablet ([ad85d01](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ad85d015f6174a1bcfa017cd4e010e91f346b020))
* Loading marketo script on layout to prevent reference errors ([755b66c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/755b66c5d7818fa2e1195ea609f50183c8a98dda))
* remove references to MGA ([736b15f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/736b15feef185dc93466c17190e1905a2fcea07c))
* remove references to MGA ([072e325](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/072e325cf2d59fbb2f2e9b9f0e04bd581c5295d5))
* Resolve "[ENG] JS Bug MktoForms2 is not defined on all pages without a form on them" ([0d9910a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0d9910ae1ff674c355d4a4df86b3615acf0e93db))
* Revert marketo fix attempt on layout tempalte ([37a58da](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/37a58da004b3997645e0097fad0bbc846ba81980))
* **search:** sitesearchresult event ([4275b8b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4275b8b18a9cdc963c55809b05de6c12b023f1be))
* **search:** sitesearchresult event tracking ([a1dfbf1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a1dfbf12958b073eff78efb6cdd93ad2e9cc3ba4)), closes [#2464](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2464)
* set Marketo Forms 2 Cross Domain request proxy in default layout ([9624af8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9624af8fe8d10f7f930c90f13a5063b1ccc9d1be))


### 🚀 Features

* "AWS re:inforce event page" ([dfa3a49](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dfa3a49f0dcc9e1f3e5494972f212a2eb2b6c37d))
* **case studies:** migrate BAB page to buyer experience ([fdc901b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fdc901bf466f90a9257df3672c3d7f230a7370b1))
* **case studies:** migrate dublin city university page to buyer experience ([58322ea](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/58322ea6477b346e4716d235c5626dab60c41a01))
* **case studies:** migrate everymatrix page to buyer experience ([be44c0c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/be44c0c717040487a8182d9ce27086e129a43053))
* **case studies:** migrate hotjar page to buyer experience ([3484c6e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3484c6e70c1ed31d14adcb0fa709ff70aa6f835d))
* **case studies:** migrate kiwi page to buyer experience ([e7fbc7b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e7fbc7b5924b64719ea89e13d5d3d76ea1b09869))
* **case studies:** migrate mpei page to buyer experience ([66a0c42](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/66a0c429fdec40f008d62dbc3af7930605a8b8d4))
* **case studies:** migrate new10 page to buyer experience ([29f48d4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/29f48d45708793ffcc848f517cc56da62814539a))
* **case studies:** migrate radiofrance page to buyer experience ([5174fcb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5174fcba65f7eeb3c484f7e640060f9b2eeaecd9))
* **customers:** migrate /customers/european-space-agency to BE ([584df51](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/584df517f2441f9b12c12be60819a87082cda9da)), closes [#2163](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2163)
* **customers:** migrate /customers/surf to BE ([e69bfa9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e69bfa93428d28cbc73c8e1061f72745cf2c3e3e)), closes [#2153](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2153)
* Deutsche Telekom case study ([aba3228](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/aba3228b1b6734e4c42db4189bb680199314e1f5))
* **Localization:** Localized Marketing Site Release ([86525c4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/86525c4fb1d021de13a84a9fb195bc97fc815147))
* Navigation release v4.0.0 ([27a98db](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/27a98db8488726885b29b4a739990eec450f7363))
* **nav:** navigation release 3.3.5 - be ([8ad0829](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8ad082993e10de56f6d41dda9f0817f8f8c337f2))
* **open-source:** Add Manjaro to open source partner roster ([e8e3ffa](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e8e3ffa516e95dfad889b233c8c83542762aa0a6))
* Remove hover states from ai cards ([08b6b8f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/08b6b8fbdac8a2a4fa1401e851b26058ba385619))
* Resolve "[ENG] /customers/anchormen to migrate to BE" ([2b7a909](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2b7a909aad6a731c7da8fc4006fe8e7137bf4044))
* Resolve "[ENG] /customers/bgs to migrate to BE" ([51fe9e3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/51fe9e3624f8aeb1ff89882cec3804a531c5b7a4))
* Resolve "[ENG] /customers/chorus to migrate to BE" ([bd3e03c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bd3e03c77d07eefe89dd4724a1b88f6c69465ba8))
* Resolve "[ENG] Add CTA in hero /customers" ([a25b6d2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a25b6d22dd141db27a87662b5526d35bcb22b224))
* **solutions:** Remove hover states from ai cards ([abee4c9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/abee4c9e754d85f561ed2aed21f1a5b9ecda3356)), closes [#2489](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2489)
* **topics:** migrate 18 topics pages to buyer experience ([6ccd167](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6ccd1679e616ad060aec51f4a401bd275267866f))
* **topics:** migrate gitlab flow topics page to buyer experience ([313e0c0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/313e0c0554c3409120914340a25a94a6d7841979))
* University of Cambridge case study ([b9f7f51](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b9f7f51549b85824823274c9bcaff64d70292da5))


### 🗂️ Content change

* add ciso to egroup ([5a929cb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5a929cbd6acbabffb74cf49a926e68a266a47455))
* **event:** Add Hack in the Box event ([520c108](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/520c108fcb15c958417b15b3f81909560ed4a67e))
* **event:** Radovan Bacovic added an event (Budapest Data Forum) ([aeaef62](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/aeaef62e98ac8591c472298c4e235097f009fdf8))
* **homepage:** Add home pill ([4279cd6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4279cd6323989953dfb08e16b13219db16079500))
* Localized  Small Business Get Started page - German, Japanese, French ([f7f4a26](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f7f4a2642f0d5d80076cd4c32c0751189bce71c2))
* Moving analytics-request issue template under MS&A ([0f3dc03](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0f3dc038bb6ea8cf6d934f6aafe507fbcb994cee))
* **partners:** add resmo to technology partners page ([b04bed7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b04bed7ec048cf031892e6bd3af502f87c8b7c7d))
* Resolve "[ENG] Marketing messaging - replace "innovate" & "modernize" buzzwords with updated language" ([34a3c73](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/34a3c73fc1d7c9319dd48cfcdb9983ea979ffaf2))
* Swap resource image ([c0c1f83](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c0c1f83608eb9a13b86a410ecd788502ec7abac2))
* update Justin's title ([f4705f1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f4705f1377ce429b9f7a979f1c3b9f03dff9fed4))
* Updates analyst relations contact info ([371bb72](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/371bb72f2c8bc2c01664a1d19725c292922b4f41))

## [1.10.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.9.0...v1.10.0) (2023-05-19)


### 🎨 Styling

* **branding:** Rename CI minutes to units of compute ([3a1fb45](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3a1fb459c151a290d621ace9c9a99b5d600ffb54))
* **branding:** Updating to units of compute ([359c694](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/359c694974e3352fe12e988757bcb800a370b309))


### ⚡ Performance Improvements

* Resolve "[ENG] Identify and replace large images from BE" ([79abe3c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/79abe3cdcd1927d9351c35e0785c04365ab16605))


### ♻️ Refactors

* Integrate schema markup generator functions + docs ([f4a7f09](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f4a7f0963935cee35ae50661188f70bfcbcccc1a))
* Remove AB test from navigation component in Buyer Experience ([b1049cd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b1049cd0f3238d3609279d5f5a6d3e362cb8e239))
* **schemas:** clean up schema generator functions + create schema documentation readme file ([68ef058](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/68ef058b90fd860a4d2082cf930d6e2b663b4a0e))


### 🗂️ Content change

* Add AI-powered to homepage here ([56039f9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/56039f9ae9ee905619b396612b7516a33e1c3f4d))
* Add audit events to compare features page ([32a25cf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32a25cfa328bc73d4c02357d56189be1083b3d7e))
* Add Lockheed quote to homepage ([a35d83f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a35d83fc3a39a2a22026a9fa0d60fd8b4c3b6875))
* Add sourcegraph to technology partners page ([4d03171](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4d031719c255fa40b11bab29c7b2b5909b78b32a))
* Added 4 AWS conferences and DOES Europe ([877899a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/877899af758825f20a3f7d483d69ae4282d141a9))
* Adding GitLab Connect San Diego to the events page. ([599b4a9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/599b4a91634c3f4e3de3a0b644ac0b7c2b3d643e))
* Adding GitLab GitOps Hands-on Workshop to the events page. ([a4ab209](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a4ab2096e63e78e3bbb26775cb28d3fb2a4c6593))
* **Free Trial:** Update localized free-trial page - German, Japanese, French ([70f7e94](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/70f7e940774a7d6f680a71d4673b1f24e8d506ee))
* Further info on grace period subscription expiry ([8970c65](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8970c65236c95a335eeb84d9c6747c0ab8ff8ba6))
* **open-source:** Add logo for partner The Good Docs Project ([11dc62b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/11dc62b4238bdb52f250564b246f8ade2cd6d05a))
* **open-source:** Refresh Kali Linux partner logo ([fbf8a24](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fbf8a24a45890f96e4302577717323ed6b62a92f))
* **Sales:** Update localized sales page - German, Japanese, French ([a89783e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a89783ea9d0d8bff1cf2d4305b7835e20545870d))
* **small business:** content updates ([21e098d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/21e098dcff7cc0df55b604d3336056304c6517e7))
* **Topics:** Add datePublished and dateModified schema attributes to topic pages ([9d9b35b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9d9b35bc619c6ef663f59150dbecff4a1ff01bee))
* Update Atlanta Address ([8b86187](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8b861875c506f0c5fca3caf43b84563f6099db27))
* Update homepage featured card to add AI ([9ecedaf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9ecedaffd5baeb9c5c7bf0511685933be5f50203))
* Update solutions security compliance link ([bcf5294](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bcf529475407ee1610b4b51ac8c243a05274f9d5))


### 🐜 Bug Fixes

* Add datalayer event to submit on /startups/join ([74fa0a9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/74fa0a93edcb67193aabacb5b4f0ec41c99cb0e9)), closes [#2286](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2286)
* Fix hreflang attribute for localized pages ([b516d4e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b516d4e97916ed2a22ca80650d480bec33c35ad8))
* **get-started:** faq show and hide all button bug ([f7a11e7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f7a11e75a1782f47d78c7a6913aa6e34a7ed0bf1))
* GTM tracking with solutions-video-modal.vue ([ecfa82e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ecfa82e324d524f44d8a29b23d42fb0d3039fc0e)), closes [#2287](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2287)
* Homepage logo resizing ([380ffa3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/380ffa3e51418b631a1f76809d63b3f162b9970a))
* **links:** Update buy links on why-gitlab ([1597840](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/15978403c2f1c30f32848a61dd0d9526f34b5004))
* **platform:** Update localized platform page hero image ([ad75841](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ad75841a45e62db13c587104d8c5398aa50f69e8))
* **redirects:** Fix the issue When a URL has params but does not have a trailing slash JS breaks ([34445aa](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/34445aa7a3add1d9d856bc02853eb8efe301d28f))
* Resolve "[ENG] /solutions/open-partners disabled component causing SEO errors" ([f15b4bf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f15b4bff5c1780c43730c8a666170fb638feaa51))
* Resolve "[ENG] BUG Goldman Sachs logo customer case study" ([7ac7842](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7ac7842d072602973d8a9f86c001b278680c17d8))
* Resolve "[ENG] BUG Goldman Sachs logo customer case study" ([dedfd84](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dedfd84b5c826ae1898e6f5e6c4a3c68eb28da08))
* **solutions:** update href on solutions card ([b6797f5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b6797f527d78180f24a8ed3ec06ed37b6f5e420a))
* spacing under searchbar ([50fb17c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/50fb17ca72a980e27abcee99c32ae532e74db7e6))
* **topics:** Fix japanese translation link topics devops platform ([b833521](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b833521a052eedfe433f780df9c32be2f0bed4c2))
* **what is gitlab video:** replace with working youtube link ([1f9abbd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1f9abbd22d9710bedc19d7c353c468e83f3b3967))


### 🚀 Features

* Added dev survey hero animation to ai page hero ([05b1014](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/05b10147f24640ecf7159ad89ffef5fdea149e26))
* Ai animation fix ([f2f8f54](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f2f8f5490e47065e930a58b54f5f29f9a3e1c024))
* Buyer Experience: Navigation release v3.3.3 ([9deba5a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9deba5a4c91083d6ee61d66110f5bf8ddb6faf3d))
* **case studies:** migrate lely page to buyer experience ([eb18fd6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/eb18fd68c46e6e4929483a8080260041bcb2831d))
* **case study:** migrate hemmersbach page to buyer experience ([4aa1720](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4aa172049c6e504d8dcb7350dcdc2d293ae31321))
* **customers:** Adds LM case study ([b373a17](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b373a1703a4bd21b1d198bc93164b2dd75b93c36))
* **customers:** migrate us_army_cyber_school to BE ([e9c192e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e9c192edb53e63be2ed77feb2fb25c644089f932)), closes [#2109](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2109)
* GitLab 16 Launch Virtual Event Landing Page ([406a124](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/406a1241274db9b6833fe2bc9c0c1346d857e266))
* Hiding Ai illustration initially to prevent it being fully displayed before animating ([a363732](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a363732b06176934ff1ff04c40da110f5240bf2a))
* **i18n:** Added get-started  ([e5e626c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e5e626cf495c54c2023c950c553967c26cf7c16e)), closes [#2370](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2370)
* **localization:** Install page tier 1 ([d92620e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d92620e2d9158192430385cfd01a667f05fba5b9))
* **nav:** release 3.3.4 ([ab77c93](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ab77c9383553a1207e01db55abedc8b6b267081e))
* Resolve "[ENG] /customers/cern to migrate to BE" ([67abca0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/67abca057a0842e9bf5979288ac489a7b3982139))
* Resolve "[ENG] New AI/ML solutions page" ([0467be3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0467be397e279e00afb2ee6fba09f0c53322c0af))
* **Solutions:** Localized Startups solution page - German,  Japanese, French ([9de2c08](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9de2c08a13f6f1b94593b6ee54ec39e9f1061b55))
* Update pubsec page with Lockheed Martin link ([7e6d99d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7e6d99d421d084509011957b4f54c387c9bd19fb))
* Updates to case study template ([4a560c0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4a560c01948ccb402b4dd96decea3061ea209bdc))

## [1.9.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.8.0...v1.9.0) (2023-05-05)


### 🎨 Styling

* **topics:** read time pill on /topics child pages ([0eb5bf1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0eb5bf1c89caa91cda351b699ad6646f5b475239)), closes [#2321](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2321)


### 🚀 Features

* **case studies:** migrate anwb page to buyer experience ([a007b83](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a007b83a9617e0ba91a226907f4cafaaef891b7b))
* **case studies:** migrate paessler-prtg page to buyer experience ([fc8b88c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fc8b88c14393618c8df6e17a658598fe76bc04ed))
* **case studies:** migrate potato london page to buyer experience ([b413695](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b4136951e4906e4f092b1448602115f0305fdd42))
* **case study:** migrate chefkoch page to buyer experience ([4070217](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4070217bbf29eefb2b6a18d36e27e3e140eb89db))
* **case study:** migrate goldman sachs page to buyer experience ([1294c1a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1294c1af27bf8c261bda6b50c168ef8327882ae8))
* **case study:** migrate hilti page to buyer experience ([9dceb37](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9dceb37bb1f9bae5e251183b3310ae9badae46f9))
* **case study:** migrate nebulaworks page to buyer experience ([25b1515](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/25b1515ecc179f0935ae7cad496442d6c22d9720))
* **case study:** migrate ow2 page to buyer experience ([e183435](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e183435708eb948d36a5ea3ff01381bc9000cb00))
* **case study:** migrate regenhu page to buyer experience ([d16af2a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d16af2ae7a078d3b117194e72e5e5c7273c94638))
* **case study:** migrate the zebra page to buyer experience ([83a064d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/83a064ddcbb12f4a13f6b75784aba19a7a451e4b))
* **case study:** migrate weave page to buyer experience ([78ba08c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/78ba08ced06efda29355c6f34e6c1d31b5ea5773))
* **case study:** migrate zoopla page to buyer experience ([2595f73](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2595f739bfcc46271dbc7b6ede384bb141af6d59))
* **customers:** Add trek10 case study ([40a94ee](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/40a94ee1e0fac1a49cffce714eb8d7963ddc1ce0)), closes [#2158](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2158)
* **Customers:** Airbus migration into Buyer Experience ([986fad3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/986fad313aa6eb73972e60fb2c129ac3032a5061))
* **Customers:** Drupalassociation migration into Buyer Experience ([259145c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/259145cb183160f4d48e96a50aab819985a37666))
* **Customers:** Fanatics migration into Buyer Experience ([0e5eae8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0e5eae858e8215bfc5fadc7be995f296774bbd18))
* **customers:** migrate /customers/axway to BE ([5a77c22](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5a77c22dd85f79ef1d4a22adaa97b357014067d9)), closes [#2159](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2159)
* **customers:** Migrate fullsave customer case study ([bc1714f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bc1714f87f18f2e1fe18271a2d42db3cb46a07be)), closes [#2128](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2128)
* **Customers:** Moneyfarm migration into Buyer Experience ([530a240](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/530a24031c0ebd4f6e0fabcd21121dc820400644))
* **free trial landing:** mvc3 - dynamic hero content based on params ([94b65de](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/94b65de3cf6b39d58d268debd14524a6f0ac8538))
* Innovation outside of R&D is not within GitLab, let's make it specific. ([995e209](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/995e209e3b35e3365d4df0c49a33523a272630b4))
* Innovation outside of R&D is not within GitLab, let's make it specific. ([32119d6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32119d65c6ec18ab42025c68726814e0fb505015))
* Localized asset on enterprise page ([9341d68](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9341d68226e9b1a2b7ed642e5ff3f91141a2c509))
* migrate /customers/deakin-university to BE ([11eb399](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/11eb3993425e2d303833d9eecae79a34971339e6)), closes [#2129](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2129)
* migrate /customers/dunelm to BE ([95707a6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/95707a61fb52fd7aab574b46b47f24644acd5cea)), closes [#2131](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2131)
* migrate /customers/haven-technologies to BE ([e225a46](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e225a460e1f824dacb4450a19fe67f2ff5a73442)), closes [#2130](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2130)
* Migrate /topics/ci-cd/continuous-integration-agile to BE ([0af092c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0af092c94f2219d0d67840e2a878949850bd2682))
* Migrate /topics/ci-cd/continuous-integration-agile to BE ([50d1496](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/50d1496f04ef51c091fcf89ccf0137aa7b0fa429))
* Migrate /topics/version-control/what-is-code-review to BE ([7147f39](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7147f395c993026330c669c4fc99f7ed84230862))
* Migrate /topics/version-control/what-is-code-review to BE ([471ca2e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/471ca2eed8bf72836718518e57765c013a7f2daf))
* Migrate /topics/version-control/what-is-gitlab-flow to BE ([da28eec](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/da28eec1ce7eac9533fb59988270fae4f639da34))
* Migrate /topics/version-control/what-is-gitlab-flow to BE ([c6c4d4c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c6c4d4c1cb28f3ad264be28e61a5c847c026c27f))
* Migrate /topics/version-control/what-is-innersource to BE ([96c1e6e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/96c1e6e9668d30d2c82d71c6ee951ef061909830))
* Migrate /topics/version-control/what-is-innersource to BE ([6116d8c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6116d8cf06f9415d969b5695c5147c63f65bca67))
* Migrate company preference center into BE ([2b3966c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2b3966c86b7b5a14dfc1a69df3496d8c7f93d7a7))
* Nav release BEx: Version 3.3.2 ([c4e6c23](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c4e6c2369d6d22d81ca98658047cf41d52634766))
* Navigation AB test with black gleam free trial button variant ([24a9d69](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/24a9d690f43add19ac3ac026b72affaba74a4e59))
* **platform:** Localized Platform page ([28b7b73](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/28b7b739d035a3a9d5bcbd5ba3edf5c134546268))
* Refresh /why-gitlab/ ([d067a80](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d067a805408b459a3dbd9c725b81e22456c37861))
* **Software Faster:** Localization of the landing page ([b235098](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b235098b2bd734c28f236e1464b15c17c8a430e3))
* Solutions landing page MVC2 ([d793615](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d79361532f503f1c0f1ca6faccaeca5e8a203dc7)), closes [#2238](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2238)
* **Topics:** Migrate "/topics/ci-cd/continuous-integration-server/" to Buyer Experience ([3d55230](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3d55230853ead2cc9324398288a9656f880c3369))


### 🗂️ Content change

* Brand Landing Page update to update Developer Survey to 2023 ([9605f68](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9605f68bf8f025e9249aba77a36ef7dc6603c4b8))
* **events:** atlanta follow up ([c8d7638](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c8d7638b38928c04feb89d52a3c97b03dc68e723)), closes [#2345](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2345)
* **pricing:** Added multiple namespace FAQ ([bff8bf8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bff8bf870dd597817603c4e2e3220166c79a22ed))
* Update FY24Q2 hackathon dates ([a07aaef](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a07aaef9d5b18a53aabb00b6b15f2d9f25d1daed))


### 🐜 Bug Fixes

* Add chrome build image to lighthouse job ([d50b9e4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d50b9e40de0f3ad50b3f98721045ffb0035b2e08))
* breadcrumb URL in world tour Atlanta page ([864587f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/864587fd9764e1160b398d2eb33ecd0331ea8de1))
* copy-media.vue regression on icons ([a4f5867](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a4f5867d98a22f15748287f78808aebb6b77f701)), closes [#2342](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2342)
* **customers:** load more link issue ([e76e8c0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e76e8c0fe3a883f0b9e015bd4e13c0c7ffe7259d))
* Fix image platform translations ([7a6d041](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7a6d0416483d8d90d84f541934aed3b5fd57c99b))
* Fixing population bug on GitLab vs GitHub ([827e517](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/827e517d6a1c7f3bd93eecf517f3795dd4063cb5))
* Resolve "Small Business Video" in resources block ([3267a5d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3267a5d6a57b99263e862269e75a0621794592af)), closes [#2296](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2296)
* **why-gitlab:** data props, cta link ([7bcdb77](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7bcdb77e37f2669f76dc270f740a17234c99c7b9)), closes [#2322](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2322)

## [1.8.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.7.0...v1.8.0) (2023-04-21)


### ♻️ Refactors

* **Software Faster:** Content and hero refresh ([4c9d180](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4c9d1806957291e8acf684a19290bb0299e069ab))


### 🎨 Styling

* **buttons:** moving partner's cta buttons ([0b95b2d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0b95b2d5c0a0d050e5823ce423e51cde1de7b7b3))
* Remove Offset property from solutions components ([7996d09](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7996d09e8446033887f8ea40cca23ce8da29afb8)), closes [#2216](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2216)


### 🚀 Features

* Add Google Lighthouse Job ([a2e7cbc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a2e7cbc0065201540e4014027512bfb3e3e9f326)), closes [#1990](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1990)
* Apply template to Topics Devops Resources page ([c6b55a5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c6b55a55de30143c20a59f1b452320c9b291e63d))
* Apply updated template to all old topics pages ([fd22ff9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fd22ff9b25164180dc8ea5f9cfa1853eb66c5133))
* **case study:** migrate hackerone to buyer experience ([b0270a5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b0270a523b042ca2222c24c7e326be0d09721048))
* **case study:** migrate nvidia page to buyer experience ([d30d5bf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d30d5bfa4cc96c3ac26392c27880dd1f1c814f86))
* **case study:** migrate siemens to buyer experience ([2b34066](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2b340660511472ab534be86b6570d045ed74e42d))
* **Company:** Company Vision - migrate to BE ([682e913](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/682e913ee2ecbaf57e5b899caa8702c0ee7099db))
* **customers:** Migrated Iron Mountain customer page ([5d04c26](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5d04c26ece7769b3b5131f68442f19a003421782))
* devsecops survey landing page ([79ac7a9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/79ac7a9b14cc9d925052a762c478995e44f7233c))
* i18n Small business page for french, german, and japanese ([db0116e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/db0116eb7cab38ee3a831131ca73739afb09af42))
* **Localization:** Localized DevOps Platform Topics page ([d782a9b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d782a9bbddb0618e8381f5e036bf6137a6086986))
* Migrate customer case study template from www ([d343235](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d343235f5cca9c3b9bff2e8a05c5f9cc22baa151)), closes [#1979](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1979)
* **migration:** Migrate board of directors page to Buyer Experience ([94a332f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/94a332f57b3c1b3774362155dd101bd513c8c58d))
* **migration:** Migrate egroup page to buyer experience ([a4f0a79](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a4f0a7939d259506c7df173ab90fd024666f4dfe))
* **migration:** Migrating company/contact to BE ([4896321](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/48963212bb00aba2c18582f8d2b3b2125156b90a))
* **nav:** Nav 3.3.1 Release ([24e9361](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/24e936160a8a9a2b1dc5b54aea4a50c4c6852b55))
* **topics:** migrate agile-methodology topics page to buyer experience ([6b131a2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6b131a24ffeb15e9c9da4ea3d229e9fd672ee870))
* **topics:** migrate agile-ppm topics page to buyer experience ([0d75972](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0d75972e6541ddad3dc8942d7cb9254083d10e82))
* **topics:** migrate ci-cd choose continuous integration tool topics page to buyer experience ([a6e20e7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a6e20e76fde0979326af30bc86fdf4ff4eaa51af))
* **topics:** migrate continuous delivery topics page to buyer experience ([a6bdfc8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a6bdfc873febb015d917809b13378ccf37542d7f))
* **topics:** migrate devsecops - what is developer first security page to buyer experience ([fa3a232](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fa3a232884c917b090e3ed656b9c024ecbe74e31))
* **topics:** migrate devsecops - what is fuzz testing topics page to buyer experience ([5b1d80b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5b1d80ba2293e9f42ef89a87197b5b6b51850d95))
* **topics:** migrate single application topics page to buyer experience ([1bb89ef](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1bb89ef73b0d57bde37632807d890ad59c6115d4))
* **topics:** migrate version control - centralized version control system topics page to buyer experience ([8a5d1c4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8a5d1c4103669ea8d9218a10cb4959b5906b31af))
* **topics:** migrate version control - what is git workflow page to buyer experience ([f67cea6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f67cea6086a1b108e29f770d65847438cff5b6b1))
* Update Topics landing page ([022302c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/022302c2827d4fc49e6147483c91b53b3ef8699e)), closes [#2091](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2091)


### 🐜 Bug Fixes

* **case study template:** change slptypography tag render to resolve html hydration issues ([c07e5de](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c07e5deb3536e9bb5b6aa79285a9bc8c72d283fe))
* **case study:** icon overflow ([17a7d9c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/17a7d9ca823c499d852bc1bd7d866965dce518dd))
* **guest calculator:** add tooltip links ([e21477c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e21477c0c9d436bea1421ecbddbcb04098de20f6))
* set FAQ jump down button as optional ([7e1001b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7e1001be6d2e5ecda246f3657edabdd80fa52067))
* svg animations on survey page ([8d0c2c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8d0c2c31a9df74a5386b59ef4f0333b873a10714))
* topics landing page URL ([01ccd01](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/01ccd0170d7193d50890fdeea71fa2b02d1f54d5))
* topics landing page URL fix ([66e1912](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/66e191226fcdfe5db380589add6c248009b5b293))


### 🗂️ Content change

* Add The Open Group to open source partners ([03a63d9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/03a63d9b907a6e40d5817e2a7e5a8103e10f1714))
* customers parent nvidia link ([9883a29](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9883a29406b6351dcd2a42c7726f8a1dc1ae9a52)), closes [#2240](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2240)
* Fix typo on solutions/education ([5f6352c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5f6352cf1a8db80a5fb8e253d98d9743826e0282))
* KubeCon EU 2023 Lightning Talk Schedule ([6ee3678](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6ee3678b0c4d5a286c22191f02c3118685c34e3f)), closes [#2193](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2193)
* Major updates to portal page ([ab50632](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ab5063242f85f8a864df9ce8ca578eaf59396e3b))
* Mention Jason as reviewer for Support pages ([31d8d69](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/31d8d690710e9e7c6ea1e6764d6fe87f9009d0b2))
* **meta:** Add Meta title and Description to YML files for /competition page and child pages ([d829ed1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d829ed15604aba7e42ae10270ca16fbde6d8a16f))
* Radovan Bacovic added: Big Data Belgrade No 3 ([eed850a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/eed850ad1db5ccf9041ad4bbf750715d27071a6f))
* Radovan Bacovic: Added AWS user group event ([10ea9d8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/10ea9d8112f80b9689772dc0d74cd57e0bdc15de))
* Remove Integration Services from content/services.yml ([05ab1ab](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/05ab1ab602b356c95fbc94f0d6ef238d09a7bb81))
* Updated the Learn page yaml file ([b4514fb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b4514fbd63597a6dafd28c3735c7c377c07e07fd))

## [1.7.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.6.0...v1.7.0) (2023-04-07)


### ⚡ Performance Improvements

* Reduce education page image sizes ([2dcb1d9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2dcb1d96e0e5b501bbec71794481773c5ebc6298))


### 🧪 Tests

* **homepage:** hero painted door abc test ([5886436](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/588643653518f350cd5fb79861a1f38eb4010ec8))


### 🐜 Bug Fixes

* **education page:** next steps component education content fix ([cedd74b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cedd74b608b840c4f98c29165504e0a9454ac085))
* **painted door test:** change order of click events for test ([bd36483](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bd364833285f0ec47569a05a52bb35e66128d9a1))
* **painted door test:** click event ([445e3c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/445e3c3b5cb271c64aac4c024462f231c190902f))


### 🗂️ Content change

* Readability improvements to migrated topic pages ([6d802b2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6d802b22616337f1695b1dbaae1e011720a109ff))
* remove IDC reprint; update contact info ([1aef99b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1aef99b4ec36c73db4f5efffcd4199a18a6254bb))
* Update intro text to resources page ([1fd0c67](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1fd0c6749c9a0675e7f2344adcd0690e44e8307d))
* Update professional services page with link to Full catalog ([7cf6cfc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7cf6cfc4d4a8bcee66e5412a82234c940c7e52db))


### 🚀 Features

* Add greenhouse job post API for careers component ([89aa8be](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/89aa8beda761efe03de668cd7a5596bedb1e1048))
* **calculators:** add personalized partner content to roi calculators ([d95ecf2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d95ecf25212659dbaac9dd21458fdde018f7375f))
* **events:** rsa page ([8c1c1bf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8c1c1bf9a4d3dcc307780cd55bd09fcb3732d5c3))
* **free trial paid search:** mvc2 ([9014ede](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9014ede3e4ac2eb8cda5e30d261d0230789145f1))
* guest users calculator ([dfa6420](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dfa642032119f116c4fea2047a9a6f12827767f6))
* **i18n:** Implementation of nuxt i18n for teamops page ([9e44aef](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9e44aef74a3bf48a9778708939f8bc71d1404842))
* Localized Enterprise page ([b220327](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b220327d8b56d9bba2d125d6d5d3758587bace3f))
* **pricing:** add reactive header text to FAQ ([8ae7878](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8ae7878a94b82c8850aaeadf510987d704605a25))
* Schema Generation for Breadcrumbs ([87f6b6b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/87f6b6bdadf399c366bee6c567284c6b6e14525c))
* **topics:** Migrate "/topics/devsecops/beginners-guide-to-container-security/" into BE ([32ab6b6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32ab6b6bd19d11a8334d281c9240fa953c441668))
* **topics:** migrate beginner devops topics page ([d28b866](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d28b8661a579300fa9a03a005e951800b86e029b))
* **topics:** migrate ci-cd pipeline as code topics page ([cea7dc1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cea7dc1fa0cc10261b5b59d25f9a832458c648c2))
* **topics:** migrate cicd pipeline topics page ([4ce7ef4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4ce7ef4b7a4f5f5cfc8c0b76fe6b9220c8051aad))
* **topics:** migrate shift left devops topics page to buyer experience ([96b2f90](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/96b2f90d5f1e8c827aaa130e45be4a18964309bb))
* **topics:** migrate version control - software team collab topics page to buyer experience ([a7a81d9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a7a81d9dae5e77376fa45d05c2432c0cb6d72eb3))


### 🎨 Styling

* fix pricing page tablet issues ([b146ca8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b146ca872f46736e636bac14a0f98c85b9605a23))
* **rebranding:** Cloud, Technology & Platform Partners ([92d4c7b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/92d4c7b3684b7674d3ca40d91b5f1fa9934dd166))
* **rebranding:** Company ([719e000](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/719e000eb66b15537828efef0b9cd95ecd6356ad))
* **rebranding:** Get Started ([ca1e444](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ca1e444a41cd8aefb0e3af140a2a5fdbded91995))
* **rebranding:** Services ([7addf13](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7addf137ebdea31fc4242554daedc4f341f0b26e))
* **rebranding:** Stages DevOps lifecycle ([73e5db4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/73e5db41d6023a588407e3d25d589dd1a93969a6))


### ♻️ Refactors

* Migrate /topics/ci-cd/continuous-integration-metrics/ page ([8465a25](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8465a254c40ee53c2587010d80e8484b81067754))
* **SEO:** Add Hreflang Attribute in localization pages ([5a3f4b9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5a3f4b989ee70c7ee0e6f84e92020cf5c459bc52))
* **software-faster:** Brand marketing landing page refresh ([3247ec0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3247ec00d68060577bf7f40e2b385a93d4c59cc9))

## [1.6.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.5.0...v1.6.0) (2023-03-24)


### 📝 Documentation

* General improvements to version control topics page ([276175a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/276175a8b4abdfcfd6965bdafac8a60a5bd37007))
* Some SEO-focused copy changes ([306466e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/306466e26697a7ef3a150f19df39dd94201fbbeb))


### 🗂️ Content change

* Add more nav images ([ad523f6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ad523f61e0c8f2ad45f2234e7029a552b98ab3c9))
* Add next hackathon event ([3308721](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/33087214f7397f9eb27eb5cbf81947e0d279cc9a))


### ♻️ Refactors

* **calculators:** consolidate shared css and logic ([2042d7b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2042d7b7fe22ab08b663e8b1b497277f1843f769))


### 🚀 Features

* "/topics/gitops/infrastructure-as-code/ migrate to BE" ([dbac2e6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dbac2e60fa8ad55099711cc6011800504d70f6e0)), closes [#2018](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2018)
* add navigation anchor tabs to pricing page faq ([244b9a7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/244b9a74148be996f298881e84935bb364b736f9))
* Add trial transfer information in Licensing FAQ ([7ba1200](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7ba120093f4a5c2f24ce99c7178dfcbbb3e7a340))
* Create All Customer Case Studies Page ([c4ef54f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c4ef54fb44bb64806eb53a17953b4d907adecabc)), closes [#1978](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1978)
* create new time roi calculator ([6821c4c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6821c4c584e74dbb79632c285bbcfe36941a3711))
* Customer Case Studies Page refresh ([109ed28](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/109ed28d8767865909eb25ccd04a58457cadc9cb)), closes [#1977](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1977)
* Eng free trial page iteration fy24q1 ([dd8b004](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dd8b0041c8c20e916ac59eac7c859536801c88eb))
* Moving topics/agile-delivery to BE ([b492ea2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b492ea25d8ebb4755b0eab545ad0cc22c8046efc))
* Navigation 3.2.0 release - SMB & Enterprise personalization ([709ee70](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/709ee70b47b403c8591e5417ecbdf4f004f72b02))
* **sales:**  Localized French and Japanese Sales Landing page ([3b56f40](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3b56f403d97ded8ee3a76d83ca6470dfc4f264db))
* **topics:** Migrate /topics/ci-cd/benefits-continuous-integration/ to BE ([14c96c8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/14c96c8a43e6719ec9f4d81a84b7f4bf022ae2bc))


### 🐜 Bug Fixes

* Change URLs to include trailing slash in customers page ([8892684](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8892684485f2b14d061a4918ea9444c3aacae5cf))
* education logo sizing ([c909f50](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c909f503816a7047d8bd05262421b30ae5a8b24d))
* Home page hero button missing padding ([0264e59](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0264e5975dfc5f03c287d2c2ba15530764c4bb68)), closes [#2071](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2071)
* **tooltips:** add slptooltip improvements to buyer experience ([fda936c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fda936c51c26a462770408a380da0166e5fdb842))
* Update opengraph image for events/kubecon ([17a4797](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/17a4797871374e9d36420bf2535277e1f7294086))
* update path of new free trial page ([5ca1f79](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5ca1f79f810fdd1c8527c587cf9c1e3497c964c9))

## [1.5.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.4.0...v1.5.0) (2023-03-10)


### 📝 Documentation

* add changelog for UX ([d03eb8a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d03eb8aea1c8b5135619b522abbc729aa2bb2b5c))


### 🐜 Bug Fixes

* **pipeline:** Avoid running the changelog on a non-release week ([503e1e5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/503e1e5b94ddd820adbd04769b97df1720a4614f))
* Update URLs and data properties that are incorrect ([b589403](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b58940328ac73c485f8f18aaf2e92fd28202ebcb))


### 🗂️ Content change

* Radovan Bacovic - 3 new events added ([05447e4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/05447e4e4a8f8e6a8baf0282c4de217639e6d757))
* Radovan Bacovic add 2 events ([012738f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/012738f9d4b20b4a8df12fc56db5378f96581060))


### 🎨 Styling

* **branding:** Company page DevOps to DevSecOps ([f719d6a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f719d6a4e2f43427302d92ccecbff42fc18dbdad))
* **branding:** Company page DevOps to DevSecOps ([b4bb684](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b4bb684ef605dd77f37bc827720a335a0d14de23))
* **branding:** DevOps to DevSecOps ([e28f383](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e28f383a27fe487ef442701a97a7cac45798cba0))
* **branding:** DevOps to DevSecOps ([40bbded](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/40bbdedcfcb417683f5a76a594313935fba8e12e))
* **branding:** Partners Page DevOps to DevSecOps ([650f75a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/650f75ac12c12655e53b403320532a51a543f6a1))
* **Rebranding:** DevOps for DevSecOps ([33dd0ee](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/33dd0ee5d0962a9676f5ac8faee7972ab8cde099))
* **Rebranding:** DevOps for DevSecOps ([752e214](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/752e214151a9d6c771263304d060dcc6f67a9674))
* **Rebranding:** DevOps for DevSecOps ([fe9dbe2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fe9dbe22deafb42e8556318c1a47304981bb37da))
* **Rebranding:** DevOps for DevSecOps ([c27c369](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c27c369e0373a06ae5334d5fdf0862ae74b6763c))
* **Rebranding:** DevSecOps for DevOps ([fae9401](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fae9401fadd3079e89fa679237d00f99065e5b06))


### ♻️ Refactors

* Buyer Experience nav release 3.1.2 ([6a382d0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6a382d017c9f8e04f2f4a0ebc0e93d03b6fcaaf4))
* Remove LaunchDarkly from nav component ([5c2c454](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5c2c454527976989038e3065eabda5ecde8e85ca))


### 🚀 Features

* Buyer Experience nav release version 3.1.3 ([9594b10](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9594b1057471164f6d6ee918c1485ee1fee8c1e7))
* Localized German Sales Landing page ([3a10ef5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3a10ef508e0e5246384db60a8cd12ad1ae0f941e))

## [1.4.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.3.0...v1.4.0) (2023-02-24)


### 🎨 Styling

* **branding:** changing DevOps to DevSecOps ([a59f1c7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a59f1c7824c7bb85d632c04d7454f04f462ea0aa))
* **branding:** changing DevOps to DevSecOps ([480f471](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/480f471d53f6974976d01580bcc444d742286fff))


### 🐜 Bug Fixes

* resolve slptypography lint errors ([560e420](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/560e420cfa69684fc80948f62959fce667de6e98))


### ♻️ Refactors

* stage icons rename  ([4cab5ae](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4cab5ae4cc9d57de831e2072fa148139bd5d4052)), closes [#1894](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1894)


### 🚀 Features

* [ENG] Update header on feature template ([c5df80b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c5df80b49cd09b34aefe95648eda3c9fa370617b)), closes [#1953](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1953)
* Add new /resources template and page ([7277427](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/72774270cf2a8eea66488b1059e2b87a4c6796cd))
* **lint:** add custom lint rule for checking slptypography for vhtml attribute ([3cda761](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3cda761cce46b28025cf25e3aacbe25312c766bd))


## [1.3.0](v1.2.0...v1.3.0) (2023-02-17)

### 🗂️ Content change

* **partners:** Add more technology partners ([7979971a](7979971a))

### ♻️ Refactors

* **carousel:** switch to using v-show and aria improvements ([60ba9c60](60ba9c60))
* Create reusable categories table component ([e0a665d1](e0a665d1))
* Remove references to pps_aggregate ([76eae192](76eae192))

### 🎨 Styling

* **branding:** DevOps to DevSecOps Tier 1 ([bc8dcc05](bc8dcc05))
* **branding:** DevOps to DevSecOps Tier 1 ([e6352243](e6352243))
* **devsecops:** changing branding from devops to devsecops ([f76b843c](f76b843c))
* **devsecops:** changing branding from devops to devsecops ([b1d8148d](b1d8148d))
* **devsecops:** changing branding from devops to devsecops ([58871845](58871845))

### 🚀 Features

* add methodology pop-up on DevOps Tools ([57cebbb4](57cebbb4)), closes [#1857](#1857)
* Add the search event listener to integrate the navigation and the BE search ([99248afa](99248afa))
* Add the search event listener to integrate the navigation and the BE search ([72107dda](72107dda))
* make entire surface area link to case study ([fc54e978](fc54e978)), closes [#1889](#1889)
* **nav release:** Navigation Release Version 3.1.0 ([80ccb357](80ccb357))
* populate why gitlab feature form using categories file ([26d464c7](26d464c7))

### 🐜 Bug Fixes

* Make resource card images proportionally consistent when resizing ([30c82afe](30c82afe)), closes [#1891](#1891)
* platform table link and order issues ([df6bb08d](df6bb08d))
* wrong URL in open source partners section ([4896b0cc](4896b0cc)), closes [#1925](#1925)


## [1.2.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.1.0...v1.2.0) (Jan 30, 2023 - Feb 12, 2023)


### ♻️ Refactors

* new engineering issue template ([e26bf48](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e26bf48938f891882c2a2ebbfee3295577bff6c4))


### 🐜 Bug Fixes

* add marketo script to pricing template pages ([01e4e00](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/01e4e009f752b246e7be6e8b83ddccb57cb5e29b))
* analyst page typography ([7433916](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/74339161fdb0b02d7a88a4ba3406d44358852ab1))
* remove unused images - part 2 ([f9db106](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f9db10623e1ce924a3407d6bfea3362f1fa2e7ec))
* **safari:** devops tools table hover state lag ([12f8d67](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/12f8d679ad9874f987f05913d246222a29fa451f)), closes [#1795](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1795)
* **solutions:** Hackerone customer logo distorted in solutions/dev-sec-ops ([0f505bb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0f505bbebe5888a08a384ae00f279b056ccb6302)), closes [#1847](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1847)
* Style changes to devops tools ([3b63c45](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3b63c451f6ef9c4df2d98b058f8788a571bd6647)), closes [#1850](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1850)
* **support links:** updated link for Instance Migration on Statement of Support page ([9e5496e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9e5496e6cd3c16cfcefd616365033cb4a8c97643))
* youtube video embeds on ci solutions page ([5dc805b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5dc805b7585e65b257a0804c8898ecf5c0ec9a27))


### 🚀 Features

* Added an event for Radovan Bacovic as a speaker ([4c1c847](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4c1c8474e992ef965ddc082a89cd806a64bb60ca))
* **competition:** change table title based on stage ([09a4fa5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/09a4fa5387bf13fde508d63e2fe0ae0062864d29)), closes [#1846](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1846)
* create showMore mixin + add showMore functionality to pricing templates ([b9d5702](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b9d57025dc628871522f92d52e5d4203740a719d))
* Make Search Results a New Page ([0ec06a0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0ec06a0f58d73d4b8bf0956a42e0eec25c4d6e8b))
* solutions/open-source page refresh ([d795d7b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d795d7ba6d266c00f03034925ce8e84f00b4e505)), closes [#1837](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1837)
* solutions/open-source/join page refresh ([9654d1f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9654d1f271be7b762306902a76108dedcbf955f1)), closes [#1840](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1840)

## [1.1.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.0.0...v1.1.0) (2023-02-03)


### ♻️ Refactors

* Analyst mvc2 ([720a697](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/720a697f32b85889820ccfb34a19474839950a92))
* **AOS:** Remove AOS in competition pages ([f82fd6b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f82fd6b964bf081279183170d647beaf3f329583))
* **AOS:** Remove AOS in partner pages ([03e860c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/03e860c77c146c23d0d9298c9fddafeded635270))
* **AOS:** Remove AOS in remaining pages ([870c045](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/870c045afdac2f68677370f04480c731ba5f09dc))
* **AOS:** Remove AOS in topic pages ([cf73866](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cf738664fc6db977372bdb246ac705be84c94767))
* **AOS:** Remove AOS on events pages ([cd779b4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cd779b4fd2483831963727bfef33e4e7db0f305b))
* **AOS:** Remove AOS on single landing pages ([e150db6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e150db65b4c2a537e48433139da39488cbff47fb))
* **AOS:** Remove AOS on solutions pages ([d89397e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d89397e0c4a3b911c8e9525d3a100c0a44f3316b))
* Change getElementByID to Vue refs ([89f9426](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/89f9426550e584fd4dc96321440967cfef12fa34))
* Create mixin that handles screen resizing ([c9ab601](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c9ab60194dabf9808cb3a3fa27d602619b53e4b5))
* **default-layout:** Add animation initialization to default layout ([f2143d8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f2143d8f1405083d9275ca19418ac04a9d9130cc))
* Remove all redundant measure units in css files ([925f262](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/925f262c2d089409d4ccdebf7f70d62a20df518a))
* Remove css invalid elements ([4d3dfa2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4d3dfa2312fa14d67a86cf9a2e0d7cdb661fd389))
* Search and remove unused images part 1 ([fffb988](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fffb988d41536e107b0682e96e28f9a4b2e180e5))


### 🎨 Styling

* add new events to events page ([fe78c08](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fe78c08f16a73b716e92585194e9365b4fcbf636))
* Added col to remove content overlapping ([c55f9b2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c55f9b2bc542e5dbcad2104d8fd44c3797ffadac))
* competition page ios safari css updates ([a2e4481](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a2e4481dcd55733603e1c005a63fb886b78970eb))
* fix border bug on competition table ([5c5c038](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5c5c038cc4e67517ed75455fa2d130126243623a))
* fix customer page filter dropdown sizing ([b30e7ca](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b30e7cab61dcebcc85ea12913a7a4dc264401bc4))
* fix horizontal scroll on select pages ([0292641](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/02926410ee29c3e4bc148a863e1606e38235ccdf))
* fix quote carousel background bug ([1ecae17](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1ecae178879b5b9af7cacf9801068c420d3ea981))
* force tier pills to stay on same line ([801a8b1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/801a8b132d76f9f78bb311b762248db7cc688822))
* **get-started:** fix typography and spacing ([3163321](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3163321818982c608db1ab8639917d98b2dda2f2))
* pricing page ios safari css fixes ([32387ea](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32387eaeb715ecdc2cab39107accbe78d9c834bc))
* styling updates for pricing template pages ([b1c0f32](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b1c0f3237beb6648619391743890906ad2910ae4))


### 🚀 Features

* Add buttons to find a partner ([c1d636a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c1d636a91f51362344007ae9950a51c57d1d780f))
* Add clarity on limits to pricing and trial page ([13318a6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/13318a6de0e4cdeac540f5a80900e7c8a0f3ca0d))
* Add Deakin University case study ([5d91007](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5d91007e58f2d9d5a3b67e610de75a6912594804))
* Add deep link into tabs on /competition page ([35022b7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/35022b7bfdbb986a5ccb51fe96e4cc212236b618)), closes [#1770](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1770)
* Add link to GitLab vs pop-ups ([ff0fea8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ff0fea836e41add08c1f72227a3167fd542c6fd2)), closes [#1733](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1733)
* add new report to CAP page ([e9ad7ed](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e9ad7ed325dc256353c9301b86d979b112897825))
* add photo to GitLab Dedicated page ([b21b8f7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b21b8f7e6341e3d3d7abd90ee6436951c10d3eb9)), closes [#1707](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1707)
* add roi calculator abc test ([9d35776](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9d35776c310d7145bbb9cdb31a402c5d75f7716d))
* Add VSD sign-up page ([48988ef](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/48988ef8f8f391a057aa7defef2ed892229bc56d)), closes [#1800](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1800)
* Added 2 events for Radovan Bacovic as a speaker ([b89eaf3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b89eaf3dd496714606d3db8fb2dbacdf42092132))
* Adding Notion, Firefly, Gitbook & AnalyticsVerse to alliance partners ([7c6f215](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7c6f215d512cd18c1c276ebb28ae883302b2056c))
* Adding Tech Talk: GitLab for Governance and Compliance to the events page ([3c18a79](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c18a79a5a2732b9bdfecae379f5700ec14f115d))
* Build business case for GitLab checklist ([bcc5dd6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bcc5dd643017da91a2807c65c1fa910b564e9488))
* **competition:** Add a hyperlink to DevOpsTools ([39513a7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/39513a77558ee8981fd1d5b47fad8f6dd2829549)), closes [#1848](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1848)
* Create brand campaign landing page ([e1e8935](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e1e893557ab39ac231de49edddfcf1a60b78176f))
* Create changelog scheduled job to run it only on "main" ([b1eddcb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b1eddcbd766baeea9905490de52a61f3ed4a5523))
* create digital transformation topics page ([6a7f6d8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6a7f6d83db7d8043b13fa17cb2a63c3213e3f88e))
* Create Direct Link to Pricing Page FAQs ([40ccd01](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/40ccd01504e9693a633e0429db63d9bc62b37154)), closes [#635](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/635)
* Deep link from /devops-tool stage to /competition tab" ([36c238e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/36c238eca27f636d101cea8904e426f99d65bc63)), closes [#1773](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1773) [#1772](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1772)
* Education solution page ([21eafd4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/21eafd4bc5331bebed60bf09fce5bf5235524506))
* **nav release:** DEX upgrade be-navigation to 2.2.0 ([b16a814](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b16a81461c5d8b397fcc52a8a6dd3942ad17dc82))
* **nav:** Nav 3.0.2 release ([549a01e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/549a01e9bab1a3aacbb4e416cd867a86523c4e7f))
* new UX for competition VS pages ([4489a29](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4489a2966511e5d3c2920389efd1907da3324296))
* Refresh TeamOps landing page with content and verbiage updates ([15b60e9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/15b60e9d61788f9c67475531c426ae3fabcf176a))
* Releasing support for notifications in the Slack app. ([9d5d342](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9d5d3420ca92e54771891bf0cc61479b59d67c13))
* remove ROI cta for same-page redirects ([3c8bcc5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c8bcc500b66e42a2bf6b8213bcd3ecf9f808d0c))
* sync platform page table up with categories.yml file ([0993613](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/09936132264ee045d7d3157744a25f597427cf60))
* update competition page UX ([2ad64ea](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2ad64ea69fe472bf7d924476acb6f4208aeed048))


### 🐜 Bug Fixes

* 404 links ([7d9832b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7d9832b87011a2cb3777b7f43488623611317244))
* add text to product analytics card ([799b329](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/799b3297871d1bfe745468f0d3b504280944cb6c))
* brand campaign devsecops image ([be5bc90](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/be5bc90d12049357ad805e7eb50f817203872535))
* change date for webcast ([f31aed9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f31aed9fc31ff2b67bee2a8a779a64fdd1bdae81))
* content: Remove "API Clients" and "CLI Clients" from the technology-partners ([e629a59](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e629a59713bc2307503fd64f3bc15fc7c1a522e8))
* Enterprise and SMB Page Loading Slow ([6528680](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6528680f5a77f81cfbd0bffccc922bb5ad9b5648))
* **gtm/ot:** unblocks gtm from ot ([1b42a3e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1b42a3e72252aaa0f1c0208ae8ffad0b4ade44cc))
* **gtm:** removing optanon class ([cf31330](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cf3133098911d06a4bdb571cecc78812b8f356f0))
* info for course to remove duplicate option ([beec9f8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/beec9f858c12d9dc13476a3371353d0facbde43f))
* Navigation increment v2.1.1 ([36b2e70](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/36b2e703c80669c5057a998fc50c4b017b19e5e3))
* **OT/GTM:** removing optanon class from GTM ([fa24bdf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fa24bdf84dce2f46fe6a04a6cdd7670d51617041))
* Project Dependency List on Feature Comparison page ([56e03e8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/56e03e8a40916b5111cbb9e7a6981dfc9dd7eb95))
* Quote block layout on mobile ([5437995](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/54379957c9883d2a17abebbf268ae6547d480833)), closes [#1810](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1810)
* random quotation mark on /features/continuous-integration/ ([0712c32](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0712c32f8e6963e48c1789c43668fbccc981d44c)), closes [#1811](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1811)
* remove entry from customers page that leads to 404 ([3d4af09](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3d4af094950bac67c8bc70686702301ddfa3b250))
* Rename side-navigation, improve reliability of scrolling events ([9c9a96e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9c9a96e4c577f5e815a782c60f576eecf465d922)), closes [#1398](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1398)
* Replace all instances of all-caps with title-case ([c223413](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c223413a75ab06f2f33f07c3eaa5f20321669734)), closes [#1563](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1563)
* Resolve "Removing partners without TPAs" ([9810ab5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9810ab5199b2eceac726b2bec0968e633eaaa419))
* **sidenav:** Source code management components displaying incorrectly ([3b1635d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3b1635dedcb391c61cd32e1573e1a6ab74368ed9)), closes [#1794](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1794)
* **sidenavvariant:** now takes up space in dom, updated relevant yml files ([e9e9225](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e9e9225ba021a11760eb30656b9a859511d489e5)), closes [#1572](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1572)
* sitemap with 2 paths to exclude ([c30ccc4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c30ccc4c73c48e5d4811a128916704de17494474))
* SlpBreadcrumb on Topics Pages ([7faa2d5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7faa2d5e259bafa23c2b93e66d0b97929c029ede)), closes [#1780](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1780)
* Small business page components have extra spacing from the sidenav ([2651843](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2651843311414fda90532298e3cb136216cbb740)), closes [#1813](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1813)
* software faster vimeo analytics are not being tracked ([3c0d01f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c0d01f5db45872dbf79b07f4b41f8f886ee1e4e))
* style of pricing button to remove extra space ([d61bca5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d61bca56de0ca4776d45f2905d8df2d6cfdf54e2))
* transparent icon on solution page card ([e820d49](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e820d496336b5938554112a93ead6fba092808da))
* typo in pricing faq ([a7d60eb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a7d60ebe8547c4cff7ac610742b4ea85e826dbf3))
* typo on gitops page ([6e3c13d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6e3c13dc26c42db892832852f8b0c101dcffac3c))
* typo on the aws-reinvent page ([553bc48](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/553bc48bcf72812d6fcd59d7ec30489a7d5eb290))
* Update colors on the TeamOps page ([f528946](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f528946dff4b5e2d2b4fbaebb6d458d1de93472e)), closes [#1753](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1753)
* update dedicated page url ([b8dfaf8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b8dfaf8320197ef508554383dc44acc5e22a2790))
* update url for product analytics docs ([959204f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/959204f61d5b3a2c703ab6e54377f7e3841bea99))
* Updates to VSM pages, Add DORA page links ([6c420f6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6c420f6dc667eb3080cafccb7e204c44f3c6487f)), closes [#1650](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1650)


### 🗂️ Content change

* Add Contributor Days Event ([a086b47](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a086b475bf33d030edb055f46868d8d245a1cbb0))
* **technology-partners:** Reclassification of technology partners ([9360c7e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9360c7e1283668eab27d6fcdb1f9901ef02f541e))
