---
  data:
    og_image: /nuxt-images/customers/open-graph-siemens.png
    twitter_image: /nuxt-images/customers/open-graph-siemens.png
    
    customer: Siemens
    customer_logo: /nuxt-images/case-study-logos/siemens-logo.png
    heading: How Siemens created an open source DevOps culture with GitLab
    key_benefits:
      - label: Improved developer efficiency
        icon: accelerate
      - label: Cost and time savings
        icon: piggy-bank-alt
      - label: Improved collaboration
        icon: user-collaboration
    header_image: /nuxt-images/blogimages/siemenscoverimage_casestudy.jpg
    customer_industry: Technology
    customer_employee_count: 25,000+
    customer_location: 190 Countries
    customer_solution: |
      [GitLab Free](/pricing/){data-ga-name="free solution" data-ga-location="customers hero"}
    sticky_benefits:
      - stat: 40,000+ 
        label: GitLab users
      - stat: 6.4M+ 
        label: builds per month
        
    blurb: Siemens uses GitLab for improved developer efficiency and customer satisfaction.
    introduction: |
      Siemens transformed its collaboration and organizational workflow with GitLab SCM, CI/CD, and DevOps.
    quotes:
      - text: We really try to bring the open source culture in, and so far, we really succeeded. With CI/CD, we have one and a half million builds every month. The whole culture has completely changed.
        author: Fabio Huser
        author_role: Software Architect at Siemens Smart Infrastructure
        author_company: Siemens
    content:
      - title: 'Worldwide innovation pioneer'
        description: |
          Siemens was founded in 1847 as a “backyard machine shop” in Berlin, Germany. Siemens AG (Berlin and Munich) is a global technology powerhouse that has stood for engineering excellence, innovation, quality, reliability, and internationality for more than 170 years. Active around the world, the company focuses on intelligent infrastructure for buildings and distributed energy systems and on automation and digitalization in the process and manufacturing industries. 

          [Siemens](https://www.siemens.com/global/en.html) brings together the digital and physical worlds to benefit customers and society. Through Mobility, a leading supplier of intelligent mobility solutions for rail and road transport, Siemens is helping to shape the world market for passenger and freight services. Via its majority stake in the publicly listed company Siemens Healthineers, Siemens is also a world-leading supplier of medical technology and digital health services. 

          In addition, Siemens holds a minority stake in Siemens Energy, a global leader in the transmission and generation of electrical power that has been listed on the stock exchange since September 28, 2020. In fiscal 2019, which ended on September 30, 2019, the Siemens Group generated revenue of €58.5 billion and net income of €5.6 billion. As of September 30, 2019, the company had around 295,000 employees worldwide on the basis of continuing operations.
      
      - title: Large-scale company with large-scale needs
        description: |
          With over 20,000 developers, Siemens is divided into multiple organizations acting within different domains, mainly focused on business-to-business initiatives. According to Fabio Huser, Software Architect, the challenge was, “How do we build a DevOps culture around this really fractured federalistic company structure?”   

          Siemens needed a DevOps platform that offered collaboration, transparency, and proper code management to achieve their goal: A community for employees around the world, and a single source of truth for code. In order for a tool to be successful, Siemens required developers to have a collaborative mindset, full stack engineering knowledge, experience as an open source contributor, and a scalable platform that can be used to build upon itself. The vision for an improved workflow included the ability to collaborate on code and share it within minutes, speed up time to market, empower people to own their own code, and set the technological foundation for future business models. 
      
      - title: Adopting open source first 
        description: |
          A small team within Siemens adopted GitLab in 2013 for collaboration and version control to develop Linux based embedded devices. In a typical grassroots approach, the team opened the platform for the whole company and scaled it up to over 40,000 users. The DevOps platform provides a place for different teams to work on the same project with the ability to share code within minutes and to collaborate easily across the world.

          “The open source world comes up with new tools every week. But at the end of the day, we really try to solve it like a human issue. We want to collaborate, and the tool is just a secondary thing after all,” Huser said. “Thanks to GitLab, we found a tool which facilitates this ideology. It's all about the people behind it and to maintain this idea and also have this community spirit within Siemens, you really need to establish such a community.” 
          
          In 2015, the code.siemens.com team shifted its focus to DevOps CI/CD, calling its specific workflow style “junkyard computing” in the early days to enable integration builds for open source components. “Thanks to the ease of use of the GitLab runner, you can set up new machines in a matter of minutes," according to Huser. "If you have old machines laying around and you have a good enough set up in terms of network, you can literally set up new runners, new capabilities in a minute. It's quite cost effective.”
      
      - title: Code, collaboration, and community
        description: |
          Today, code.siemens.com has its [IT infrastructure on AWS](/blog/2020/03/24/from-monolith-to-microservices-how-to-leverage-aws-with-gitlab/){data-ga-name="infrastructure on aws" data-ga-location="customers content"}. There is no longer a need for “junkyard computing” because code.siemens.com is a fully established service with a large in-house developer community provided by the Siemens IT organization. 
        
          The infrastructure evolved to a highly tuned and sophisticated setup, with a large amount of EC2 instances all managed as [Infrastructure as Code](/topics/gitops/infrastructure-as-code/){data-ga-name="infrastructure as code" data-ga-location="customers content"}. SaaS solutions such as S3, RDS, ElastiCache, EFS, and ELB are used as well, since those can be replaced by standard open source solutions to minimize vendor lock-in. GitLab is hosted on AWS, also the supporting services such as GitLab CI runners, monitoring, logging, crash reporting and more.  Siemens has exceeded over 38 million CI builds since adopting GitLab. “If you're part of Siemens you have different repositories you can collaborate with. We really try to bring the open source culture in and so far, we really succeeded. With CI/CD we have one and a half million builds every month. The whole culture has completely changed,” Huser said.   
          
          With GitLab, Siemens saves both time and money because there is no need to maintain local patches or manually update fixes. The code.siemens.com team follows an ‘upstream first’ workflow. “We go without patches. We only deploy upstream versions, nothing else. If we want to have new features, we contribute them to GitLab. We do not patch our instance,” said Roger Meier, Principal Key Expert and Service Owner of code.siemens.com from Siemens IT. “As soon as they are merged upstream, we will deploy the next version. So we ship every month. We do about four production deployments per month.”  
          
          The code.siemens.com platform is managed by a team of just eight people distributed across four countries in a highly agile fashion. All team members are committed to the open source way of working. They are coaching, supporting, and guiding the internal developer community, on top of managing the whole infrastructure and application. They use GitLab day by day to manage all their activities. All team members contribute and/or maintain several open source projects, while providing a reliable service for the wider Siemens developer community to increase developer happiness.
          
          Collaboration happens throughout the entire organization with over 40,000 GitLab users and the potential to expand. GitLab helps Siemens ensure scalability internally and with customer development opportunities. “Our customers and our developers just want to have a reliable service that is running all the time,” Meier added.
          
          Siemens teams heavily contribute to GitLab with over 150 merged MRs in GitLab. In addition, Huser and Meier are [GitLab Heroes](/community/heroes/){data-ga-name="heros" data-ga-location="customers content"} and were selected as [GitLab MVPs](https://about.gitlab.com/community/mvp/){data-ga-name="mvps" data-ga-location="customers content"}. The teams not only use the DevOps platform, but they pride themselves with being so knowledgeable that they don’t use a support team from GitLab. “Since the beginning, we were talking about all our ideas and to have our roadmap visible for all the people within the company. You have to walk the talk, that's key. Of course, focus on your customers: for developers, from developers,” Meier said.
