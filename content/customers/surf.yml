---
  data:
    title: SURF
    description: How SURF increased deployment speed by 1,400%
    og_title: SURF
    twitter_description: How SURF increased deployment speed by 1,400%
    og_description: How SURF increased deployment speed by 1,400%
    og_image: /nuxt-images/blogimages/surf.jpg
    twitter_image: /nuxt-images/blogimages/surf.jpg

    customer: SURF
    customer_logo: /nuxt-images/logos/surf.svg
    heading: How SURF increased deployment speed by 1,400%
    key_benefits:
      - label: Improved version control
        icon: merge
      - label: Faster deployment time
        icon: automated-code
      - label: Improved collaboration
        icon: collaboration
    header_image: /nuxt-images/blogimages/surf.jpg
    customer_industry: Science and Research
    customer_employee_count: 300
    customer_location: Amsterdam
    customer_solution: |
      [GitLab Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: decrease in deploy times
        stat: 93%
      - label: min to create a product environment cluster
        stat: 25
      - label: more MRs daily
        stat: 2x
    blurb:  Dutch research cooperative SURF was looking for a single source of truth for the entire organization to help them improve their IT processes and better organize projects.
    introduction: |
      With GitLab, SURF found versatility and user management features that other solutions didn't provide — allowing SURF to deliver solutions to research and education communities much faster.
    quotes:
      - text: |
          Because of CI/CD, development and operations teams have a framework of co-operation. This is probably the highest value GitLab has introduced in our teams.
        author: Giuseppe Gianquitto
        author_role: Cloud Lead Architect
        author_company: Research and Development Services
    content:
      - title: IT supporting science research
        description: |
          Formed in 1974, SURF is a cooperative association that supports the national scientific and educational communities and institutions in the Netherlands and their worldwide partners like CERN. Within the field of research, SURF provides IT solutions, platforms, and architectures for data management, analytics, and computation to its members.


          [SURF](https://userinfo.surfsara.nl/) supports over one hundred institutions between medical research centers, universities, research institutes and space exploration centers across the nation. Driving innovation together. That is the mission of SURF. Together with the institutions, SURF ensures that the education and research community has access to the finest and newest Information and Communication Technologies (ICT) facilities for top research and talent development.

      - title: Lacking version control and collaboration
        description: |
          SURF was previously using several tools in a very scattered environment. Each team was using different platforms in various ways, including SBC, GitHub, and GitLab CE. With so many different on-premises installations per group, developers were disconnected from one another and lacked the ability to truly collaborate.


          SURF needed a solution that could act as the single source of truth for the entire organization. Teams needed one solution for user management and granular repository access. The goal was to provide an improvement of processes, an overall organization of projects, and enhanced user management capabilities.

      - title: More than a version control system
        description:  |
          SURF was previously using GitLab primarily as a Git tool. As they researched tools for version control, they discovered that GitLab Premium offers versatility and user management features that other solutions don’t provide.


          SURF adopted GitLab Premium as a versioning platform to run on premises. Teams started by using GitLab for version control and then formed a step-by-step process for [CI adoption](/topics/ci-cd/implement-continuous-integration/).

      - title: Increased deployment, code management, and IaC
        description: |
          Deployment times have decreased tremendously, allowing SURF to deliver solutions to research and education communities much faster. “We are coming from a place where you buy the hardware, you rock it, you plug it, you install the operating system. From this, we moved directly to deployment using GitLab CI for infrastructure as a code. Deploys went from two weeks to one day. So it took two weeks to deploy a cluster. Now it can take one day if it's a really, really complex case. That's the worst case scenario,” said Giuseppe Gianquitto, cloud lead architect, Research and Development Services.


          Workflow is simplified not only because of the [GitLab Flow](/topics/version-control/what-is-git-workflow/), but also because of CI/CD templates, which empowers teams to leverage Infrastructure as Code (IaC) and deploy complex systems in a streamlined and repeatable fashion. “GitLab is the backbone of our deployments, from data analytics to IoT distributed platforms. We rely on GitLab to rapidly create complex solutions,” said Machiel Jansen, head of Scalable Data Analytics group.


          The Research and Development Services unit within SURF designs and deploys Kubernetes clusters as IaC with applications spread across dozens of repositories using a modular and pluggable design and with GitLab as the backbone. “We deploy Kubernetes clusters for our developers. So those clusters, they come up with everything ready for the developer to create an application, production-ready for the final user. Now, the entire process for us to the product cluster is less than 25 minutes,” said Gianquitto. “From the moment that the developer says, ‘I need a new cluster, because I need to go into production with the municipality of Amsterdam or with a project for CERN,’ the environment is ready within 25 minutes.”


          GitLab runners are responsible for authenticating to the cloud environment, and typically run Terraform and Ansible deployments to set up the required infrastructures and applications. Over 20 Kubernetes clusters and over 300 nodes are serving universities and institutes with complex workloads.


          The microservices running on those Kubernetes clusters, from internal platforms to the end services, are designed across GitLab repositories. Creating, scaling, and deleting those applications and infrastructures is entirely driven by GitLab CI. “Our DevOps teams can provision production-ready solutions within minutes, from Kubernetes to multi-cloud and multi-tier complex hybrid cloud infrastructures. GitLab CI/CD in synergy with Terraform gives us all we need to provide our researchers and data scientists with reliable and re-usable tools,” Gianquitto said.


          SURF now has a single platform for Git, version control, code management, IaC, and CI/CD. Developer throughput has increased and cross-functional relationships have improved. “Mostly because CI/CD, development and operations teams have a framework of cooperation. This is probably the highest value GitLab has introduced in our teams,” Gianquitto said.
