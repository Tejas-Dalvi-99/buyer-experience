---
  data:
    title: U.S. Army Cyber School
    description: The U.S. Army Cyber School created secure, collaborative coursework with GitLab CI/CD, DevOps, and SCM.
    og_title: U.S. Army Cyber School
    twitter_description: The U.S. Army Cyber School created secure, collaborative coursework with GitLab CI/CD, DevOps, and SCM.
    og_description: The U.S. Army Cyber School created secure, collaborative coursework with GitLab CI/CD, DevOps, and SCM.
    og_image: /nuxt-images/blogimages/us-army-cyber-school.jpeg
    twitter_image: /nuxt-images/blogimages/us-army-cyber-school.jpeg
    customer: U.S. Army Cyber School
    heading: How the U.S. Army Cyber School created “Courseware as Code” with GitLab
    key_benefits:
      - label: Improved collaboration
        icon: collaboration
      - label: Enhanced CI integration
        icon: continuous-integration
      - label: Easy template creation
        icon: clipboard-check-alt
    header_image: /nuxt-images/blogimages/us-army-cyber-school.jpeg
    customer_industry: Government
    customer_employee_count: 200+
    customer_location: Fort Gordon, Georgia, U.S.
    customer_solution: |
      [GitLab Ultimate](/pricing/ultimate/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: faster review cycles
        stat: 36x
      - label: commits by students and contributors
        stat: 8,415
      - label: courses created using GitLab
        stat: 6
    blurb: The U.S. Army Cyber School wanted to build its software development using Git and continuous integration (CI) and found success with GitLab’s all-in-one solution.
    introduction: |
      The U.S. Army Cyber School created secure, collaborative coursework with GitLab continuous integration and delivery (CI/CD), DevOps, and source code management (SCM).
    quotes:
      - text: |
          Instead of having to teach people 10 different things, they just learn one thing, which makes it much easier in the long run.
        author: Chris Apsey
        author_role: Captain
        author_company: U.S. Army
    content:
      - title: The U.S. Army Cyber Center of Excellence
        description: |
          The [U.S. Army Cyber School](https://cybercoe.army.mil/CYBERSCH/index.html) is responsible for the functional training and education of all U.S. Army 17 Series Soldiers. The U.S. Army Cyber School provides students with the required knowledge, skills, and abilities related to Defensive Cyberspace Operations (DCO), Offensive Cyberspace Operations (OCO), Electronic Warfare (EW), and cyberspace planning, integration, and synchronization.
      - title: Starting Git from scratch
        description: |
          After being established in 2014, the U.S. Army Cyber School at Fort Gordon was tasked with creating the school’s software development process from the ground up. There wasn’t a legacy system to manage or update, but at the same time, the school had nothing to work from — no instructors, no content, and no playbook to follow.

          As the team started to create content for the program, the data was primarily created and stored on individual laptops. That approach quickly became problematic when the school experienced turnover and content was lost when individuals left the organization. Other early tactics included emailing around Zip files and whiteboarding, which resulted in an inefficient and exceedingly manual process. The school also lacked a way for the team to collaborate and track progress on their projects. Without a common repository for the team to contribute to, the school risked losing the valuable content they paid contractors to develop.

          To address their growing challenges, the school drew inspiration from other teams doing software development and began their search for a Git-based solution for their unique situation. The goal was to empower the team with “Courseware as Code” and use DevOps principles like CI/CD to replace the traditional maintenance of content in documentation styles (presentations, word processing documents, spreadsheets, etc.) with markdown language and CI pipelines.

          Captain Christopher Apsey and his team wanted one software tool that would allow developers to collaborate on a single corpus of information, a platform for students and staff to use Git and provide a way to [access CI pipelines](/blog/2019/07/12/guide-to-ci-cd-pipelines/){data-ga-name="continuous integration" data-ga-location="customers content"}. They researched GitHub, Gogs, Node Kitten, and Gitea, and ultimately chose GitLab because it offered a variety of services that other platforms didn’t match, including integrated CI. With the collaborative capabilities of GitLab, coursework and certification assessments were established.
      - title: From a Zip drive to CI pipeline
        description: |
          The U.S. Army Cyber School uses their GitLab instance to administer and grade the programming exam, the first certification assessment for 17 Series Developers in the Army. The CI pipeline is used for grading exams, which is an interactive JavaScript website hosted on GitLab pages that summarizes test results. The process is actively being developed in order to streamline grading and certification through automation and collaboration.

          To achieve a successful Courseware as Code deployment, nearly every piece of technical content the school has curated in GitLab has been written in markdown language. This includes student templates, course content, slide decks, and handouts. All of the content production has been CI driven and is completed in the same fashion as software development. Whenever someone makes a change to a document, it is recorded and stored within GitLab as the [single source of truth](/topics/version-control/){data-ga-name="single source of truth" data-ga-location="customers content"}.
      - title: SCM, CI/CD, and DevOps for career success
        description: |
          Through the implementation of GitLab’s automated workflow, the U.S. Army Cyber School has established coursework for multi-instructor, multi-contributor, location-disparate classes and has solved many of the limitations that they previously experienced. There have been six courses created using GitLab and over 4,000 merges between instructors and students.

          Because everyone contributes within the central repository, no one leaves the school with government information, which had formerly been an exfiltration risk. The school is also benefiting from alumni who can contribute their knowledge from the field and provide real-time field updates. Graduates are encouraged to shape the learning material for current students by submitting merge requests directly into the course repositories.

          With DevOps, SCM, and CI/CD firmly in place, all contributors work in a collaborative and transparent environment. Issues, boards, epics, and checklists are heavily used as data tools to increase student participation. These GitLab features allow instructors to easily track how students are progressing. Traditional documentation formats have been replaced with GitLab’s trackable project repositories. “We can now look and say, ‘Who’s doing what? Who’s value-added? Who’s being a team player, who’s slacking off?’ all within the GitLab commit history,” CPT Apsey said.

          Exams are created using a group-level template with the seed from a student’s examination file. The exam template includes a merge request and issues, so when a new project is created the student has their own repository. “We add them as a developer to the repository and they make commits to any branches they want and merge them into develop or they can make commits straight to develop. It’s from that open MR where we have the conversation and grade,” said CPT Jessie Lass, Senior Developer, U.S. Army.

          Review cycle times previously took three years, but with GitLab’s asynchronous collaboration it now moves much faster. “In GitLab I’ve been able to tag people and then within a week get feedback. I wouldn’t say it’s a full formalized review cycle, but certainly from years down to months,” according to CPT Benjamin Allison, Cyberspace Operations Officer.

          Using GitLab has not only improved the internal development of examinations, but it is also helping students become proficient in Git. “My hope is that these candidates, once we certify them, they’ll have had a little bit of exposure to what a healthy professional development workflow looks like. So when they show up onto a team, they’re actually ready to work,” CPT Jessie Lass said.
