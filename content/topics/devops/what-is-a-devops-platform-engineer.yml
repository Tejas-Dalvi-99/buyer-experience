---
  title: What is a DevOps platform engineer?
  description: "A bleeding-edge role, DevOps platform engineers fill the space between hardware and software."
  partenttopic: devops
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  date_published: 2023-03-23
  date_modified: 2023-03-30
  topics_header:
      data:
        title:  What is a DevOps platform engineer?
        block:
          - metadata:
              id_tag: what-is-devops
            text: |
              A bleeding-edge role, DevOps platform engineers fill the space between hardware and software.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: DevOps
      href: /topics/devops/
      data_ga_name: devops
      data_ga_location: breadcrumb
    - title: What is a DevOps platform engineer?
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: What is a DevOps platform engineer?
          href: "#what-is-a-dev-ops-platform-engineer"
          data_ga_name: What is a DevOps platform engineer?
          data_ga_location: side-navigation
          variant: primary
        - text: Platform engineers have serious skills
          href: "#platform-engineers-have-serious-skills"
          data_ga_name: Platform engineers have serious skills
          data_ga_location: side-navigation
        - text: How platform engineers work in a DevOps environment
          href: "#how-platform-engineers-work-in-a-dev-ops-environment"
          data_ga_name: How platform engineers work in a DevOps environment
          data_ga_location: side-navigation
    content:
      - name: topics-copy-block
        data:
          header: What is a DevOps platform engineer?
          blocks:
            - column_size: 8
              text: |
                In the complex world that can be software development, a [DevOps platform](/solutions/devops-platform/) that can be deployed as a single application brings all the disparate forces together. It’s tough to argue against the benefits of a single solution, but someone has to “own” the platform.

                In this new era of DevOps, enter the DevOps platform engineer, a bleeding-edge role that is popping up on various job listings. Although some would argue a platform engineer is just another name for a [site reliability engineer](/topics/devops/what-is-a-site-reliability-engineer/), the rise of [cloud native](/topics/cloud-native/) technologies such as Kubernetes, [microservices](/topics/microservices/), and containers have pushed some companies to create a [platform engineering team](/topics/devops/how-and-why-to-create-devops-platform-team/) (or teams) charged with overseeing the platforms and related technologies.
      - name: topics-copy-block
        data:
          header: Platform engineers have serious skills
          blocks:
            - column_size: 8
              text: |
                A look at advertised job responsibilities and qualifications shows how a platform engineer is expected to operate in a DevOps team. In general, a platform engineer’s role is to help developers get software out the door more quickly and with security in mind. As such, it’s not surprising companies are looking for platform engineers with:

                * CI/CD and other automation experience
                * Familiarity with infrastructure as code
                * Familiarity with cloud native technologies such as Kubernetes, Google Cloud, and Docker
                * Extensive experience with cloud deployments
                * Knowledge of secure coding practices including OWASP, secrets management, and vulnerability remediation
                * Strong programming chops and deep familiarity with Linux/Unix operating systems
                * Solid interpersonal skills and a desire to improve the developer experience

                It’s important to remember, however, that the responsibilities of a platform engineer could vary widely depending on the type of organization. A greenfield company with no legacy systems is likely to have cloud expertise baked in, while an enterprise (and its presumptive legacy systems) may need extra help when it comes to migrations.
      - name: topics-copy-block
        data:
          header: How platform engineers work in a DevOps environment
          blocks:
            - column_size: 8
              text: |

                There’s no question that a platform engineer plays a pivotal role sitting between Dev and Ops, but leaning more toward operations. One company [wrote about their DevOps platform engineering journey](https://medium.com/seek-blog/platform-engineering-why-we-dont-need-a-devops-team-e88c8b97cc4f) and said at the end of the day their focus was on operations and site reliability. Others have suggested a DevOps platform engineer must be responsible for seamless “self-serve” production for developers, as well as monitoring, alerting, and even potentially evangelism for the platform itself.

                It is likely this role will continue to evolve over time as more teams adopt DevOps platforms and take full advantage of them. In our [2022 Global DevSecOps Survey](/developer-survey/), three-quarters of respondents told us their teams use a DevOps platform or plan to use one this year. Another 21% said they are considering a DevOps platform in the next two to three years.
  components:
    - name: solutions-resource-cards
      data:
        title: More on DevSecOps platforms
        column_size: 4
        cards:
          - icon:
              name: video
              variant: marketing
              alt: Video Icon
            event_type: Video
            header: The benefits of a single application explained in a video (2.5 minutes)
            image: /nuxt-images/blogimages/axway-case-study-image.png
            link_text: "Learn more"
            href: https://www.youtube.com/embed/MNxkyLrA5Aw
            data_ga_name:
            data_ga_location: body
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: Case Study
            header: Give devs the power to deploy and double your efficiency
            image: /nuxt-images/blogimages/autodevops.jpg
            link_text: "Learn more"
            href: /customers/glympse/
            data_ga_name:
            data_ga_location: body
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: Blog
            header: How the idea of a DevOps platform was born in Poland
            image: /nuxt-images/blogimages/scm-ci-cr.png
            link_text: "Learn more"
            href: /blog/2020/10/29/gitlab-hero-devops-platform/
            data_ga_name:
            data_ga_location: body
