---
  title: What is GitOps?
  description:  GitOps is a process of automating IT infrastructure using infrastructure as code and software development best practices such as Git, code review, and CI/CD pipelines.
  date_published: 2022-02-09
  date_modified: 2023-03-23
  topic_name: GitOps
  icon: gitlab-cd
  topics_header:
      data:
        title: What is GitOps?
        block:
            - text: |
                  GitOps is an operational framework that takes DevOps best practices used for application development such as version control, collaboration, compliance, and CI/CD, and applies them to infrastructure automation.
              link_text: "Download the GitOps Ebook Now"
              link_href: https://page.gitlab.com/resources-ebook-beginner-guide-gitops.html
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: GitOps
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: What is GitOps?
          href: "#what-is-git-ops"
          data_ga_name: what-is-git-ops
          data_ga_location: side-navigation
          variant: primary
        - text: How do teams put GitOps into practice?
          href: "#how-do-teams-put-git-ops-into-practice"
          data_ga_name: how do teams put git ops into practice
          data_ga_location: side-navigation
        - text: GitOps challenges
          href: "#git-ops-challenges"
          data_ga_name: git ops challenges
          data_ga_location: side-navigation
        - text: GitOps benefits
          href: "#git-ops-benefits"
          data_ga_name: git ops benefits
          data_ga_location: side-navigation
        - text: What is the difference between GitOps and DevOps?
          href: "#what-is-the-difference-between-git-ops-and-dev-ops"
          data_ga_name: what is the difference between git ops and dev ops
          data_ga_location: side-navigation
        - text: Key components of a GitOps workflow
          href: "#key-components-of-a-git-ops-workflow"
          data_ga_name: key components of a git ops workflow
          data_ga_location: side-navigation
    hyperlinks:
      text: ''
      data: []
    content:
      - name: 'topics-copy-block'
        data:
          header: What is GitOps?
          column_size: 10
          blocks:
              - text: |
                  While much of the software development lifecycle has been automated, infrastructure has remained a largely manual process that requires specialized teams. With the demands made on today’s infrastructure, it has become increasingly crucial to implement infrastructure automation. Modern infrastructure needs to be elastic so that it can effectively manage cloud resources that are needed for continuous deployments.

                  Modern and cloud native applications are developed with speed and scale in mind. Organizations with a mature DevOps culture can deploy code to production hundreds of times per day. DevOps teams can accomplish this through development best practices such as version control, code review, and CI/CD pipelines that automate testing and deployments.

                  GitOps is used to automate the process of provisioning infrastructure, especially modern cloud infrastructure. Similar to how teams use application source code, operations teams that adopt GitOps use configuration files stored as code (infrastructure as code). GitOps configuration files generate the same infrastructure environment every time it’s deployed, just as application source code generates the same application binaries every time it’s built.
      - name: 'topics-copy-block'
        data:
          column_size: 10
          header: How do teams put GitOps into practice?
          blocks:
              - text: |
                  GitOps is not a single product, plugin, or platform. There is no one-size-fits-all answer to this question, as the best way for teams to put GitOps into practice will vary depending on the specific needs and goals of the team. However, some tips on how to get started with GitOps include using a dedicated GitOps repository for all team members to share configurations and code, automating the deployment of code changes, and setting up alerts to notify the team when changes occur.

                  GitOps requires three core components:

                  ### IaC:
                  GitOps uses a [Git repository](/blog/2020/11/12/migrating-your-version-control-to-git/){data-ga-name="Git repository" data-ga-location="body"} as the single source of truth for infrastructure definitions. Git is an open source version control system that tracks code management changes, and a Git repository is a .git folder in a project that tracks all changes made to files in a project over time. [Infrastructure as code (IaC)](/topics/gitops/infrastructure-as-code/){data-ga-name="IaC" data-ga-location="body"} is the practice of keeping all infrastructure configuration stored as code. The actual desired state may or may not be not stored as code (e.g., number of replicas or pods).

                  ### MRs:
                  GitOps uses merge requests (MRs) or pull requests (PRs) as the [change mechanism](/blog/2020/10/13/merge-request-reviewers/){data-ga-name="Change mechanism" data-ga-location="body"} for all infrastructure updates. The MR or PR is where teams can collaborate via reviews and comments and where formal approvals take place. A merge commits to your main (or trunk) branch and serves as an audit log or audit trail.

                  ### CI/CD:
                  GitOps automates infrastructure updates using a Git workflow with [continuous integration and continuous delivery (CI/CD)](/topics/ci-cd/). When new code is merged, the CI/CD pipeline enacts the change in the environment. Any configuration drift, such as manual changes or errors, is overwritten by GitOps automation so the environment converges on the desired state defined in Git. GitLab uses CI/CD [pipelines](/blog/2020/12/02/pre-filled-variables-feature/) to manage and implement GitOps automation, but other forms of automation, such as definitions operators, can be used as well.
                video:
                  video_url: https://www.youtube.com/embed/JtZfnrwOOAw
      - name: 'topics-copy-block'
        data:
          column_size: 10
          header: GitOps challenges
          blocks:
            - text: |
                With any collaborative effort, change can be tricky and GitOps is no exception. GitOps is a process change that will require discipline from all participants and a commitment to doing things in a new way. It is vital for teams to write everything down.

                GitOps allows for greater collaboration, but that is not necessarily something that comes naturally for some individuals or organizations. A GitOps approval process means that developers make changes to the code, create a merge request, an approver merges these changes, and the change is deployed. This sequence introduces a “change by committee” element to infrastructure, which can seem tedious and time-consuming to engineers used to making quick, manual changes.

                It is important for everyone on the team to record what’s going on in merge requests and issues. The temptation to edit something directly in production or change something manually is going to be difficult to suppress, but the less “cowboy engineering” there is, the better GitOps will work.
      - name: 'topics-copy-block'
        data:
          header: GitOps benefits
          column_size: 10
          blocks:
            - text: |
                There are many benefits of GitOps, including improved efficiency and security, a better developer experience, reduced costs, and faster deployments.

                With GitOps, organizations can manage their entire infrastructure and application development lifecycle using a single, unified tool. This allows for greater collaboration and coordination between teams and results in fewer errors and faster problem resolution.

                In addition, GitOps can help organizations take advantage of containers and microservices and maintain consistency across all their infrastructure — from Kubernetes cluster configurations and Docker images to cloud instances and anything on-prem.
      - name: 'topics-copy-block'
        data:
          header: What is the difference between GitOps and DevOps?
          column_size: 10
          blocks:
            - text: |
                There are a few key differences between GitOps and DevOps. For one, GitOps relies heavily on automation and tooling to manage and deploy code changes, while DevOps focuses more on communication and collaboration between teams. Additionally, GitOps is typically used in conjunction with containerization technologies like Kubernetes, while DevOps can be used with any type of application.

                GitOps is a branch of DevOps that focuses on using Git code repositories to manage infrastructure and application code deployments. The main difference between the two is that in GitOps, the Git repository is the source of truth for the deployment state, while in DevOps, it is the application or server configuration files.
      - name: 'topics-copy-block'
        data:
          header: Key components of a GitOps workflow
          column_size: 10
          blocks:
            - text: |
                There are four key components to a GitOps workflow, a Git repository, a continuous delivery (CD) pipeline, an application deployment tool, and a monitoring system.

                1. The Git repository is the source of truth for the application configuration and code.

                2. The CD pipeline is responsible for building, testing, and deploying the application.

                3. The deployment tool is used to manage the application resources in the target environment.

                4. The monitoring system tracks the application performance and provides feedback to the development team.
      - name: topics-cta
        data:
          title: What makes GitOps work?
          text: |
            As with any emerging technology term, GitOps isn’t strictly defined the same way by everyone across the industry. GitOps principles can be applied to all types of infrastructure automation including VMs and containers, and can be very effective for teams looking to manage Kubernetes-based infrastructure.

            While many tools and methodologies promise faster deployment and seamless management between code and infrastructure, GitOps differs by focusing on a developer-centric experience. Infrastructure management through GitOps happens in the same version control system as the application development, enabling teams to collaborate more in a central location while benefiting from Git’s built-in features.
  components:
    - name: solutions-resource-cards
      data:
        title: Related Resources
        column_size: 4
        grouped: true
        cards:
          - icon:
              name: articles
              variant: marketing
              alt: Articles Icon
            event_type: "Next step"
            header: Learn how GitLab streamlines GitOps workflows
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-devops.png"
            href: "/solutions/gitops/"
            data_ga_name: Learn how GitLab streamlines GitOps workflows
            data_ga_location: resource cards
          - icon:
              name: articles
              variant: marketing
              alt: Articles Icon
            event_type: "Next step"
            header: What does infrastructure as code mean?
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
            href: /topics/gitops/infrastructure-as-code/
            data_ga_name: What does infrastructure as code mean?
            data_ga_location: resource cards
          - icon:
              name: articles
              variant: marketing
              alt: Articles Icon
            event_type: "Next step"
            header: Why GitLab’s collaboration technology is critical for GitOps
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-cicd.png"
            href: /topics/gitops/gitops-gitlab-collaboration/
            data_ga_name: Why GitLab’s collaboration technology is critical for GitOps
            data_ga_location: resource cards
          - icon:
              name: articles
              variant: marketing
              alt: Articles Icon
            event_type: "Next step"
            header: How teams use GitLab and Terraform for infrastructure as code
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-gitlab.png"
            href: /topics/gitops/gitlab-enables-infrastructure-as-code/
            data_ga_name: How teams use GitLab and Terraform for infrastructure as code
            data_ga_location: resource cards
          - icon:
              name: articles
              variant: marketing
              alt: Articles Icon
            event_type: "Next step"
            header: Multicloud deployment for GitOps using GitLab
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-gitops.png"
            href: /topics/gitops/gitops-multicloud-deployments-gitlab/
            data_ga_name: Multicloud deployment for GitOps using GitLab
            data_ga_location: resource cards
          - icon:
              name: articles
              variant: marketing
              alt: Articles Icon
            event_type: "Next step"
            header: The benefits of GitOps workflows
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-devsec.png"
            href: /topics/gitops/gitops-best-practices/
            data_ga_name: The benefits of GitOps workflows
            data_ga_location: resource cards
          - icon:
              name: articles
              variant: marketing
              alt: Articles Icon
            event_type: "Next step"
            header: What is a GitOps workflow?
            image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
            link_text: "Learn more"
            href: /topics/gitops/gitops-workflow/
            data_ga_name: What is a GitOps workflow?
            data_ga_location: resource cards
          - icon:
              name: video
              variant: marketing
              alt: Video Icon
            event_type: "Video"
            header: What is GitOps? Why is it important? How can you get started?
            link_text: "Watch now"
            image: "/nuxt-images/topics/what-is-gitops.jpeg"
            href: https://www.youtube.com/embed/videoseries?list=PL05JrBw4t0KoixDjLVMzKwVgx0eNyDOIo
            data_ga_name: What is GitOps? Why is it important? How can you get started?
            data_ga_location: resource cards
          - icon:
              name: video
              variant: marketing
              alt: Video Icon
            event_type: "Video"
            header: Using GitLab for GitOps to break down silos and encourage collaboration
            link_text: "Watch now"
            image: "/nuxt-images/topics/using-gitlab-for-gitops.jpeg"
            href: https://www.youtube.com/embed/videoseries?list=PLFGfElNsQthbno2laLgxeWLla48TpF8Kz
            data_ga_name: Using GitLab for GitOps to break down silos and encourage collaboration
            data_ga_location: resource cards
          - header: '[Expert Panel Discussion] GitOps: The Future of Infrastructure Automation'
            link_text: Learn more
            image: "/nuxt-images/resources/resources_1.jpeg"
            event_type: Webcast
            icon:
              name: webcast
              variant: marketing
              alt: Webcast Icon
            href: /why/gitops-infrastructure-automation/
            data_ga_name: '[Expert Panel Discussion] GitOps: The Future of Infrastructure Automation'
            data_ga_location: resources
          - header: Managing infrastructure through GitOps with GitLab and Anthos
            link_text: Learn more
            image: "/nuxt-images/resources/resources_2.jpeg"
            event_type: Webcast
            icon:
              name: webcast
              variant: marketing
              alt: Webcast Icon
            href: /webcast/gitops-gitlab-anthos/
            data_ga_name: Managing infrastructure through GitOps with GitLab and Anthos
            data_ga_location: resources
          - header: GitLab and HashiCorp - A holistic guide to GitOps and the Cloud Operating Model
            link_text: Learn more
            image: "/nuxt-images/resources/resources_3.jpg"
            event_type: Webcast
            icon:
              name: webcast
              variant: marketing
              alt: Webcast Icon
            href: /webcast/gitlab-hashicorp-gitops/
            data_ga_name: GitLab and HashiCorp - A holistic guide to GitOps and the Cloud Operating Model
            data_ga_location: resources
          - header: Automating cloud infrastructure with GitLab and Terraform
            link_text: Learn more
            image: "/nuxt-images/resources/resources_4.jpeg"
            event_type: Webcast
            icon:
              name: webcast
              variant: marketing
              alt: Webcast Icon
            href: /webcast/gitops-gitlab-terraform/
            data_ga_name: Automating cloud infrastructure with GitLab and Terraform
            data_ga_location: resources
          - header: A beginner's guide to GitOps
            link_text: Learn more
            image: "/nuxt-images/resources/resources_10.jpeg"
            event_type: Book
            icon:
              name: book
              variant: marketing
              alt: Book Icon
            href: https://page.gitlab.com/resources-ebook-beginner-guide-gitops.html
            data_ga_name: A beginner's guide to GitOps
            data_ga_location: resources
    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - header: How to use GitLab and Ansible to create infrastructure as code
            icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            text: |
                Explore the power of GitLab CI as we demo Ansible playbooks in infrastructure as code.
            link_text: Learn more
            href: /blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/
            image: /nuxt-images/blogimages/gitlab-ansible-cover.png
          - header: Optimize GitOps workflow with version control from GitLab
            icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            text: |
                  A GitOps workflow improves development, operations and business processes and GitLab’s CI plays a vital role.
            link_text: Learn more
            href: /blog/2019/10/28/optimize-gitops-workflow/
            image: /nuxt-images/blogimages/gitops-image-unsplash.jpg
          - header: Why GitOps should be the workflow of choice
            icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            text: |
              What is GitOps and how do you apply it in real-world applications?
            link_text: Learn more
            href: /blog/2020/04/17/why-gitops-should-be-workflow-of-choice/
            image: /nuxt-images/blogimages/shiro-hatori-WR-ifjFy4CI-unsplash.jpg


