---
  title: GitLab for Startups
  description: The single application to accelerate your startup. We are offering our top tiers for free to startups that meet the eligibility criteria. Learn more!
  image_alt: GitLab for Startups
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  GitLab for startups
        title: Accelerate your growth
        subtitle: Release software faster while strengthening security with The DevSecOps Platform
        aos_animation: fade-down
        aos_duration: 500
        title_variant: 'display1'
        mobile_title_variant: 'heading1-bold'
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
          text: Start your free trial
          data_ga_name: Start your free trial
          data_ga_location: header
        secondary_btn:
          url: /solutions/startups/join/
          text: Join the GitLab for Startups Program
          data_ga_name: startup join
          data_ga_location: header
          icon:
            variant: product
            name: chevron-lg-right
        image:
          image_url: /nuxt-images/solutions/startups_hero.jpeg
          alt: "Image: gitlab for open source"
          bordered: true
    - name: customer-logos
      data:
        text_variant: body2
        companies:
        - image_url: "/nuxt-images/logos/chorus-color.svg"
          link_label: Link to Chorus customer case study
          alt: "Chorus logo"
          url: /customers/chorus/
        - image_url: "/nuxt-images/logos/zoopla-logo.png"
          link_label: Link to zoopla customer case study
          alt: "zoopla logo"
          url: /customers/zoopla/
        - image_url: "/nuxt-images/logos/zebra.svg"
          link_label: Link to the zebra customer case study
          alt: "the zebra logo"
          url: /customers/thezebra/
        - image_url: "/nuxt-images/logos/hackerone-logo.png"
          link_label: Link to hackerone customer case study
          alt: "hackerone logo"
          url: /customers/hackerone/
        - image_url: "/nuxt-images/logos/weave_logo.svg"
          link_label: Link to weave customer case study
          alt: "weave logo"
          url: /customers/weave/
        - image_url: /nuxt-images/logos/inventx.png
          alt: "inventx logo"
          url: /customers/inventx/

    - name: 'side-navigation-variant'
      links:
        - title: Overview
          href: '#overview'
        - title: Benefits
          href: '#benefits'
        - title: Customers
          href: '#customers'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:

            - name: 'solutions-video-feature'
              data:
                header: Speed. Efficiency. Control.
                description: For startups, deploying software faster and more efficiently isn’t a nice-to-have. It’s the difference between growing your customer base, reaching revenue goals, and differentiating your product in the market — or not. GitLab is The DevSecOps Platform built to accelerate your growth.
                video:
                  url: 'https://player.vimeo.com/video/784035583?h=d978ef89bf&color=7759C2&title=0&byline=0&portrait=0'

        - name: 'div'
          id: benefits
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Why startups choose GitLab
                light_background: false
                large_card_on_bottom: true
                cards:
                  - title: One platform for the entire lifecycle
                    description: With a single application for the entire software delivery lifecycle, your teams can focus on shipping great software — not maintaining numerous toolchain integrations. You’ll improve communication, end-to-end visibility, security, cycle times, onboarding, and much more.
                    href: /platform/?stage=plan
                    data_ga_name: platform plan
                    icon:
                      name: cycle
                      alt: cycle Icon
                      variant: marketing
                  - title: Deploy to any environment or cloud
                    description: GitLab is cloud-neutral, so you have the freedom to use it how and where you want — giving you the flexibility to change and add cloud providers as you grow. Deploy seamlessly to AWS, Google Cloud, Azure, and beyond.
                    icon:
                      name: merge
                      alt: merge Icon
                      variant: marketing
                  - title: Built-in security
                    description: With GitLab, security and compliance are built into every stage of the software delivery lifecycle, enabling your team to identify vulnerabilities earlier and making development faster and more efficient. As your startup grows, you’ll be able to manage risk without sacrificing speed.
                    href: /solutions/security-compliance/
                    data_ga_name: solutions dev-sec-ops
                    icon:
                      name: devsecops
                      alt: devsecops Icon
                      variant: marketing
                  - title: Create and deploy software faster
                    description: GitLab enables you to decrease cycle times and deploy more frequently with less effort. Do everything from planning new features to automated testing and release orchestration, all in a single application.
                    href: /platform/
                    data_ga_name: platform
                    icon:
                      name: speed-gauge
                      alt: speed-gauge Icon
                      variant: marketing
                  - title: Stronger collaboration
                    description: Break down silos and empower everyone in your company to collaborate around your code — from development, security, and operations teams to business teams and non-technical stakeholders. With The DevSecOps Platform, you can increase visibility across the entire lifecycle and help improve communication for everyone working together to move software projects forward.
                    icon:
                      name: collaboration-alt-4
                      alt: Collaboration Icon
                      variant: marketing
                  - title: Open source platform
                    description: GitLab’s open core gives you all the benefits of open source software, including the opportunity  to leverage the innovations of thousands of developers worldwide continuously working to improve and refine it.
                    icon:
                      name: open-source
                      alt: open-source Icon
                      variant: marketing
                  - title: GitLab scales with you
                    description: From seed funding to cash flow positive to large multinational enterprise (or wherever you’re headed), GitLab will grow with you.
                    href: /customers/
                    data_ga_name: customers
                    cta: See some of GitLab's customers
                    icon:
                      name: auto-scale
                      alt: auto-scale Icon
                      variant: marketing
        - name: 'div'
          id: customers
          slot_enabled: true
          slot_content:
            - name: 'education-case-study-carousel'
              data:
                header: How we helped Startups like yours
                customers_cta: true
                cta:
                  href: '/customers/'
                  text: View more stories
                  data_ga_name: customers
                case_studies:
                  - logo_url: "/nuxt-images/logos/chorus-color.svg"
                    institution_name: Chorus
                    quote:
                      img_url: /nuxt-images/blogimages/Chorus_case_study.png
                      quote_text: Life is just so much easier for product engineering and anyone who wants to interact with product engineering because they can just do it through GitLab
                      author: Russell Levy
                      author_title: Co-founder and CTO, Chorus.ai
                    case_study_url: /customers/chorus/
                    data_ga_name: chorus case study
                    data_ga_location: case study carousel
                  - logo_url:  "/nuxt-images/logos/hackerone-logo.png"
                    institution_name: hackerone
                    quote:
                      img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
                      quote_text: GitLab is helping us catch security flaws early and it's integrated it into the developer's flow. An engineer can push code to GitLab CI, get that immediate feedback from one of many cascading audit steps and see if there's a security vulnerability built in there, and even build their own new step that might test a very specific security issue.
                      author: Mitch Trale
                      author_title: Head of Infrastructure, HackerOne
                    case_study_url: /customers/hackerone/
                    data_ga_name: hackerone case study
                    data_ga_location: case study carousel
                  - logo_url: /nuxt-images/logos/anchormen-logo.svg
                    institution_name: Anchormen
                    quote:
                      img_url: /nuxt-images/blogimages/anchormen.jpg
                      quote_text: You can really focus on the work. You don’t need to think about all these other things, because once you’ve had GitLab set up, you have basically this safety net. GitLab protects you, and you can just really focus on the business logic. I would definitely say it improves operational efficiency.
                      author: Jeroen Vlek
                      author_title: CTO,  Anchormen
                    case_study_url: /customers/anchormen/
                    data_ga_name: anchormen case study
                    data_ga_location: case study carousel


    - name: cta-block
      data:
        cards:
          - header: See if the GitLab for Startups Program is right for you.
            icon: increase
            link:
              text: GitLab for Startups Program
              url: /solutions/startups/join/
              data_ga_name: startup join
          - header: How much is your current toolchain costing you?
            icon: piggy-bank-alt
            link:
              text: ROI calculator
              url: /calculator/roi/
              data_ga_name: roi calculator
