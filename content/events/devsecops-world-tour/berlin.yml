---
  title: "Berlin World Tour"
  og_title: Berlin World Tour
  description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  og_description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  twitter_description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  og_image: /nuxt-images/events/world-tour/cities/Berlin illustration.png
  twitter_image: /nuxt-images/events/world-tour/cities/Berlin illustration.png
  time_zone: Central European Summer Time (CEST)
  hero:
    breadcrumbs:
      - title: DevSecOps World Tour
        href: /events/devsecops-world-tour/
      - title: Berlin
    header: Berlin
    subtitle: October 19, 2023
    location: |
      Spreespeicher 
      Stralauer Allee 2
      10245 Berlin, Germany
    image:
      src: /nuxt-images/events/world-tour/cities/Berlin illustration.png
    button:
      text: Register for Berlin
      href: '#registration-form'
      variant: secondary
      icon:
        name: arrow-down
        variant: product
        size: sm
  agenda:
    - time: 8:30 am
      name: Registration & Breakfast
      speakers:
       
    - time: 9:30 am
      name: |
        Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps
      speakers:
        - name: Ashley Kramer
          title: Chief Marketing and Strategy Officer
          company: GitLab
          image:
            src:  /nuxt-images/events/world-tour/speakers/ashley-kramer.jpg
          biography:
            text: |
              Ashley Kramer is GitLab’s Chief Marketing and Strategy Officer. Ashley leverages her leadership experience in marketing, product, and technology to position GitLab as the leading DevSecOps platform. She also leads the strategy for product-led growth and code contribution to the GitLab platform.

              Prior to GitLab, Ashley was CPO and CMO of Sisense and has held several leadership roles, including SVP of Product at Alteryx and Head of Cloud at Tableau, as well as marketing, product, and engineering leadership roles at Amazon, Oracle, and NASA.
      description:
        title: 'Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps'
        text: |
          Join us for the GitLab DevSecOps World Tour in Berlin and get ready to be inspired by the latest trends and innovations. You will gain a comprehensive overview of the evolving Dev,Sec and Ops landscape, to help you stay ahead of the curve.  


          As businesses continue to grapple with the challenges posed by an ever-changing economic climate, it is crucial to stay abreast of the latest developments to help you accelerate faster. Obtain an in depth understanding of how DevSecOps has transformed the software development lifecycle across organizations, and how you can confidently secure your software supply chain, while being laser focused on delivering business value.

    - time: 10:00 am
      name: Next up for The DevSecOps Platform + Product Demo
      speakers:
        - name: Mike Flouton
          title: VP, Product Management
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Mike Flouton.jpg
          biography:
            text: Mike Flouton, VP of Product Management at GitLab, leads the CI/CD, technical enablement, and SaaS product teams. With nearly 25 years in cybersecurity, cloud, SaaS and enterprise software, Mike previously served as VP of Products at Barracuda Networks and VP of Product Marketing at BAE Systems. He is also an angel investor and startup advisor. A lifelong technology enthusiast, Mike began his career as a software engineer and holds a BS from Cornell University.
      
        - name: Fabian Zimmer
          title: Director, Product Management - SaaS Platforms
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Fabian Zimmer.jpg
          biography:
            text: Fabian has a background in Bioinformatics where he analyzed large amounts of genomic and transcriptomic data. He is passionate about building products that solve interesting problems and enjoys working in a fast-paced environment. At GitLab, Fabian is responsible for GitLab's SaaS Platforms section, including GitLab.com and GitLab Dedicated. The SaaS Platforms section aims to create tools, services, and frameworks that make building and deploying GitLab a delightful experience on any hardware at any scale.
        
      description:
          text: |
            Teams are facing more pressure than ever to deliver software faster. Software development processes are undergoing transformative changes too. Join us to hear about our vision for the GitLab DevSecOps platform, take a deep dive into our latest innovations geared towards helping you build secure, efficient and reliable software and learn how you can keep security at the forefront of your growth. 
    - time: 10:45 am
      name: Break
    - time: 11:00 am
      name: |
         In conversation with Mike Johann, Deutsche Telekom IT GmbH
      speakers:
        - name: Stephen Walters
          title: Field CTO
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Stephen Walters.png
          biography:
            text: Stephen is a highly experienced Subject Matter Expert in Value Stream Management, DevSecOps, ITIL, SDLC and Agile with management & consultancy experience across end-to-end IT disciplines since 1992. He is certified in Value Stream Management, DevOps, SAFe, CMMI, ITIL, TOGAF and Prince2. As an Ambassador for the DevOps Institute and Influencer with the Value Stream Management Consortium, Stephen has provided keynotes at conferences and webcasts and is most recently the co-author of a paper on Value Stream Reference Architecture.
        - name: Michael Johann
          title: Principal Platform Engineer
          company: Deutsche Telekom IT GmbH
          biography:
            text: Michael joined Deutsche Telekom IT in 2019 after being 30 years a freelancer and founder. He has been a primary contributor to MagentaCICD which is based on GitLab and serves thousands of developers. Since 2023 he has been a core contributor to the Telekom Internal Developer Platform and focuses on hands-on and architecture.
          image: 
            src: /nuxt-images/events/world-tour/speakers/Michael Johann.jpg
      description:
          text: Join us for this fireside chat and hear from Deutsche Telekom IT GmbH on how they successfully transformed their DevOps processes. You will gain insights on how they approached roadblocks and challenges which in turn helped them to emerge victorious in their DevSecOps journey. You will also learn how they effectively implemented DevSecOps best practices into their process to help their teams work smarter and deliver software faster. In this unique opportunity we also dig deeper into how leading organizations respond to rapidly evolving business needs and economic conditions, with minimal impact to their delivery velocity.
    - time: 11:45 pm
      name: Lunch & Networking
    - time: 01:00 pm
      name: |
        **Workshop:** How to optimize your DevSecOps workflow by focusing on value streams
      speakers:
        - name: Jonathan Fullam
          title: VP of Global Solutions Architecture
          company: GitLab
          biography:
            text: Currently, Jonathan leads a global team of value delivery and technical experts as Vice President of Solutions Architecture at GitLab, focusing on helping enterprises drive their business-oriented outcomes by realizing new capabilities and efficiencies within their operating model, technology strategy, and software delivery and management processes.
          image:
            src: /nuxt-images/events/world-tour/speakers/Jonathan Fullam.jpeg

        - name: Simon Mansfield
          title: Senior Manager, Solutions Architecture
          company: GitLab
          biography:
            text: |
              Simon is a seasoned DevOps and value stream expert with over 17 years in the tech industry. As the Senior Manager of Solutions Architecture at GitLab, he specializes in optimizing software development and delivery processes through cutting-edge DevOps practices. A sought-after speaker, Simon frequently shares his insights on DevOps, automation, and CI/CD pipelines at international conferences. Connect with him on LinkedIn and Twitter for his latest thoughts on the future of software development.
          image:
            src: /nuxt-images/events/world-tour/speakers/Simon Mansfield.jpg

        - name: Sujeevan Vijayakumaran
          title: Solutions Architect
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Sujeevan Vijayakumaran.jpg
          biography:
            text: |
              Sujeevan Vijayakumaran joined GitLab in 2020 as a Solutions Architect with a focus on Enterprise customers in Germany, Austria and Switzerland.

              He's also an author of books in German about Git and DevOps, a speaker at conferences and a podcaster.
       
        - name: Jan Kunzmann
          title: Solutions Architect
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Jan Kunzmann.jpeg
          biography:
            text: |
              Jan is a science and technology nerd - and a seasoned Solutions Architect at GitLab. He has a strong focus on observability, big data, security, and privacy and enjoys philosophizing about the impact of technology on our society. When he's not looking at a screen, you might find him sitting at his piano improvising Jazz tunes.
      description:
        text: |
          If your team is looking to optimize your DevOps practices, join the DevOps Value Stream experts for valuable insights and actionable strategies. This interactive workshop will focus on discovery, identification and removal of blockers to drive visibility and continuous improvement. Dive deeper into Value Stream Assessments (VSA) and uncover areas of improvement in the SDLC. You will learn how you can accelerate value streams for faster business value realization. This interactive exercise will also provide practical experience for you to apply these VSA methodologies to your teams to identify key areas for improvement. At the end of this session, you will have a solid understanding of VSA principles, practical tools and techniques to set you ahead on the path to improving your DevOps processes.
    - time: 2:30 pm
      name: Break
    - time: 2:45 pm
      name: |
        Bringing GitLab to Work
      speakers:
        - name: Nico Meisenzahl
          title: Head of DevOps Consulting & Operations
          company: white duck GmbH
          image:
            src: /nuxt-images/events/world-tour/speakers/Nico Meisenzahl.jpg
          biography:
            text: Nico Meisenzahl works as Head of DevOps Consulting & Operations at white duck. As Cloud Solution Architect, an elected Microsoft MVP and GitLab Hero, his current passion is for topics around Cloud-Native and Kubernetes. Nico is a frequent speaker at conferences, user group events, and Meetups in Europe and the United States.
      description:
          text: Join us for a conversation with Nico Meisenzahl, a GitLab Hero and open source advocate. Discover why Nico loves the GitLab DevSecOps platform, how he contributes as a community leader, and what empowers developers to push their ideas to new heights. Engage with this true advocate of collaborative development in this inspiring conversation and learn more about how GitLab fosters innovation with the community.
    - time: 3:10 pm
      name: |
        **Roundtable discussion:** The human element of DevSecOps
      description:
          text: |
            ‘Technology isn’t the hard problem anymore, culture seems to be the chief problem now.’- Is this a statement that you have come across?

            We invite you to discuss with leaders the role and impact of people on DevOps processes. Dig deeper into why communication, collaboration and empathy play a critical role in effective DevOps strategies. What are the strategies that work while introducing DevOps into businesses. Does it work if you find the core problem the organization faces and start there? Is education on agile a great place to start when thinking about building a DevOps culture? Join us and weigh in on the best practices to foster teamwork, promote accountability and  develop a human-centered design for DevOps processes.

    - time: 3:40 pm
      name: Closing Remarks
      speakers:
        - name: Diane O'Neal
          title: Director, Product & Solutions Marketing at GitLab
          company: GitLab
          image:
            src: /nuxt-images/sixteen/diane-o-neal-picture.png
    - time: 4:00 pm
      name: Snacks + Networking Reception
  form:
    header: Register for DevSecOps World Tour in Berlin
    confirmation:
      - Thank you for registering!
      - Please check your email for a confirmation email.
    form_id: '3662'
  conduct_banner:
    header: Code of conduct
    text: GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](/company/culture/ecoc/) to ensure DevSecOps World Tour is a friendly, inclusive, and comfortable environment for all participants.
