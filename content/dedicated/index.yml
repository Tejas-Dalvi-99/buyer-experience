---
  title: 'GitLab Dedicated'
  og_title: 'GitLab Dedicated'
  description: The speed, efficiency and security of a DevSecOps platform available as a single-tenant SaaS.
  twitter_description: The speed, efficiency and security of a DevSecOps platform available as a single-tenant SaaS.
  og_description: The speed, efficiency and security of a DevSecOps platform available as a single-tenant SaaS.
  og_image: /nuxt-images/dedicated/open-graph-gitlab-dedicated.jpeg
  twitter_image: /nuxt-images/dedicated/open-graph-gitlab-dedicated.jpeg
  side_navigation_links:
    - title: Overview
      href: '#overview'
    - title: Benefits
      href: '#benefits'
    - title: Customers
      href: '#customers'
    - title: Analysts
      href: '#analysts'
    - title: Resources
      href: '#resources'
  side_navigation_button:
    text: Contact sales
    url: '/sales/'
    data_ga_name: sales
    data_ga_location: navigation
  hero:
    title: GitLab Dedicated
    subtitle: GitLab Enterprise DevSecOps Platform as a single-tenant SaaS deployment
    primary_btn:
      text: Contact sales
      url: '/sales/'
      data_ga_name: sales
      data_ga_location: hero
    image:
      image_url: /nuxt-images/dedicated/gitlab-dedicated-hero.png
      image_url_mobile: /nuxt-images/dedicated/gitlab-dedicated-hero.png
      alt: "Image: GitLab Dedicated"
      bordered: false
      image_purple_background: false
  overview_cards:
    header: A single-tenant DevSecOps platform
    cards:
      - title: The flexibility and efficiency of a SaaS solution
        text: GitLab fully manages and deploys the DevSecOps platform in a region of your choice, enabling you to focus on achieving speed, agility, and efficiencies
      - title: The control and compliance of a self-hosted solution
        text: GitLab Dedicated enables you to meet complex compliance standards by offering full data and source code IP isolation, data residency, and private networking
  by_solution_benefits:
    title: An enterprise DevSecOps platform with no maintenance overhead
    is_accordion: true
    items:
      - icon:
          name: server
          alt: server Icon
          variant: marketing
        header: Fully managed and deployed by GitLab
        text: Zero platform maintenance overhead with access to the latest software versions and security updates
      - icon:
          name: increase
          alt: increase Icon
          variant: marketing
        header: High availability and scalability
        text: Cloud native architecture with support of up to 50K users and disaster recovery plan to meet your specific needs
      - icon:
          name: report
          alt: report Icon
          variant: marketing
        header: Full control over your data
        text: Bring your own key to encrypt data stored in GitLab
      - icon:
          name: pin
          alt: pin Icon
          variant: marketing
        header: Data residency in your region of choice
        text: 30+ [AWS regions](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#aws-regions-not-supported) supported
      - icon:
          name: code
          alt: code Icon
          variant: marketing
        header: Full data and source code IP isolation
        text: Meet regulatory and compliance requirements
      - icon:
          name: shield-check-light
          alt: shield Icon
          variant: marketing
        header: Data protection and security through private networking
        text: Establish secure communications with your DevSecOps platform
      - icon:
          name: devsecops
          alt: devsecops Icon
          variant: marketing
        header: Comprehensive DevSecOps
        text: All the capabilities of GitLab Ultimate essential for organization-wide security, compliance, and planning in a dedicated deployment model - completely managed by GitLab
  
  education_case_study_carousel:
    header: GitLab is trusted by more than 50% of the Fortune 100
    customers_cta: true
    cta:
      href: '/customers/'
      text: View all customer stories
      data_ga_name: customers
    case_studies:
      - logo_url: "/nuxt-images/logos/natwest-group.svg"
        institution_name: NatWest
        quote:
          quote_text: "NatWest Group is adopting GitLab Dedicated to enable our engineers to use a common cloud engineering platform; delivering new customer outcomes rapidly, frequently and securely with high quality, automated testing, on demand infrastructure and straight-through deployment. This will significantly enhance collaboration, improve developer productivity and unleash creativity via a 'single-pane-of-glass' for software development."
          author: Adam Leggett
          author_title: Platform Lead - Engineering Platforms, NatWest
  report_cta:
    layout: "dark"
    id: '#analysts'
    title: Analyst reports
    reports:
    - description: "GitLab recognized as the only Leader in The Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
    - description: GitLab recognized as a Leader in the 2023 Gartner Magic Quadrant" for DevOps Platforms
      url: /gartner-magic-quadrant/
    data_ga_name: analyst report
    data_ga_location: body
  stats_timeline:
    header: >
      Forrester study: 
      GitLab enabled 427% ROI
    cta:
      href: https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html
      text: Read the study
      data_ga_name: forrester study
      data_ga_location: body
    stats:
      - icon: piggy-bank-alt
        stat: 427%
        text: return on investment
      - icon: increase
        stat: 12x
        text: increase in number of annual releases
      - icon: continuous-delivery
        stat: 87%
        text: improved development and delivery efficiency      
      - icon: calendar-alt-3
        stat: <6 months
        text: payback period
  solutions_cards:
    column_size: 4
    layout: 'light'
    title: Resources
    link:
      text: Explore more resources
      url: /resources/
      data_ga_name: all resources
      data_ga_location: resource cards
    cards:
      - icon:
          name: whitepapers
          variant: marketing
          alt: whitepaper Icon
        category: "Brief"
        title: "Compliant + Flexible: GitLab Dedicated"
        description: ''
        link_text: "Learn more"
        href: https://cdn.pathfactory.com/assets/10519/contents/518918/f4c1a71e-7a90-495c-b7e3-eb44bfb423a1.pdf
        data_ga_name: Compliant and flexible
        data_ga_location: resource cards
      - icon:
          name: blog
          variant: marketing
          alt: blog Icon
        category: "Blog"
        title: "GitLab Dedicated: Single tenant SaaS is generally available"
        description: ''
        link_text: "Learn more"
        href: http://about.gitlab.com/blog/2023/06/15/gitlab-dedicated-available/
        data_ga_name: blog
        data_ga_location: resource cards
      - icon:
          name: report
          variant: marketing
          alt: report Icon
        category: "Report"
        title: 2023 Global DevSecOps Report Series
        description: ''
        link_text: "Learn more"
        href: /developer-survey/
        data_ga_name: devsecops report
        data_ga_location: resource cards
  cta_block:
    cards:
      - header: GitLab Dedicated Direction
        icon: news
        link:
          text: Learn More
          url: /direction/saas-platforms/dedicated/
          data_ga_name: direction
          data_ga_location: body
      - header: GitLab Dedicated Documentation
        icon: docs-alt
        link:
          text: Learn More
          url: https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/
          data_ga_name: documentation
          data_ga_location: body
