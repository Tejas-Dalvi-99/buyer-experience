---
  title: GitLab Partners Benefits
  description: GitLab has developed a robust set of partner enablement, training and commercial programs to enable our ecosystem of partners and customers to gain the full benefits of DevSecOps and Digital Transformation investments.
  components:
    - name: 'hero'
      data:
        title: Benefits for Partners
        text: GitLab has developed a robust set of partner enablement, training and commercial programs to enable our ecosystem of partners and customers to gain the full benefits of DevSecOps and Digital Transformation investments.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Apply today
          url: https://partners.gitlab.com/English/register_email.aspx
        image:
          url: /nuxt-images/solutions/infinity-icon-cropped.svg
          alt: "Image: gitlab partners"
    - name: 'topics-copy-block'
      data:
        header: Overview
        column_size: 9
        top_margin: slp-mt-md-64
        blocks:
          - text: |
              The GitLab Global Partner Program enables current and potential partners — including systems integrators, cloud and hyper-scalers, resellers, distributors, managed service providers, and technology/ISV partners — to maximize the value of their DevSecOps expertise and the GitLab platform for their customers.

              Partners are a critical element of GitLab's mission to enable our customers with modern software-driven experiences, and to ensure "Everyone Can Contribute" through a robust and thriving partner ecosystem that cultivates innovation and stimulates transformation.

              Together with our partners we help businesses and organizations of all sizes and verticals, across the globe, to lead the digital transformation necessary to operate more effectively while providing a great customer experience.
    - name: 'topics-copy-block'
      data:
        header: Benefits
        column_size: 9
        blocks:
          - text: |
              Strong partnerships are the foundation for successful collaboration and innovation. As a GitLab Partner, you'll have access to the resources, tools, and support you need to accelerate business growth and provide greater customer value through solution offerings that are integrated and interoperable with GitLab.
              
              To support your growing needs, GitLab has created several programs to support our partner's growth and success. These provide:  
    - name: 'partners-showcase'
      data:
        padding: 'slp-mt-32 slp-mt-md-48'
        items:
          - title: Product sales discounts, referral fees and other incentives
            icon:
              name: handshake
              alt: Handshake Icon
              variant: marketing
          - title: Not for resale and discounted internal use product licenses
            icon:
              name: access
              alt: Access Icon
              variant: marketing
          - title: Technical guidance and consultative assistance through certifications, design guides and field support
            icon:
              name: community
              alt: Community Icon
              variant: marketing
          - title: Co-marketing funds to drive thought leadership and build pipeline
            icon:
              name: pipeline-alt
              alt: Pipeline Icon
              variant: marketing  
          - title: And even more benefits based on your partner type
            icon:
              name: increase
              alt: Increase Icon
              variant: marketing
    - name: 'topics-copy-block'
      data:
        header: Channel Program for Resell, Integration and Training Partners
        column_size: 9
        blocks:
          - text: |
              Companies need skilled, qualified experts to help them implement and adopt the latest DevSecOps technologies, with training and enablement of best practices to improve the quality, security and speed of software development. GitLab offers the industry's true single DevSecOps platform that offers end to end software development and operations lifecycle in a single product with one UI and data mode. This means that GitLab Channel Partners can offer their customers the most complete solution on the market and help them develop efficient, integrated end to end DevSecOps processes. The result? Simplified development toolchains, faster delivery of quality software, reduced operational costs, and improved security and compliance.

              And it’s not just the completeness of the solution that creates opportunities for GitLab partners. By joining the GitLab Partner Program, you will be partnering with a company that is rapidly growing annual recurring revenue and global customer base. As the GitLab customer base grows, our partners’ business opportunities for product license sales and services grow.  
            link:
              url: https://about.gitlab.com/handbook/resellers/
              text: See the GitLab Channel Partner Handbook
              data_ga_name: see the gitlab channel partner handbook                 
    - name: 'topics-copy-block'
      data:
        header: Alliance Program for Cloud and Technology Partners
        column_size: 9
        blocks:
          - text: |
              We collaborate with industry-leading cloud and technology providers across all major industries to deliver the best curated modern DevSecOps platform.  Our partners integrate with GitLab to deliver customized DevSecOps solutions across industries and use cases.   
            link:
              url: /partners/technology-partners/
              text: Find an Alliance Partner
              data_ga_name: find an alliance partner       
    - name: 'quotes-carousel'
      data: 
        header: Testimonials
        full_width: true
        no_background: true
        no_padding: true
        decoration: true
        quotes:
          - quote: |
              “Software and applications are now the heart of any business and the basis for digital transformation, innovation and efficiency. GitLab will be at the center of our complementary portfolio of integrated tools and services which allow teams to work better together using a united workflow and bridging silos and stages to deliver more value to our channel partners and their customers with less friction and much faster. We can now help our channel partners with their customers’ software innovation challenges by offering a full cloud and infrastructure independent enterprise CI/CD solution with governance, control and speed.”
            author: Mohamed Yassini
            job: CEO at Amazic Group
            url: /blog/2021/010/04/ubs-gitlab-devops-platform/
          - quote: |
              “GitLab’s new partner program will further accelerate the public sector’s ability to achieve its digital transformation initiatives by enabling agencies to optimize their code development and delivery with best-of-breed enterprise open source technology. We look forward to working with GitLab, our resellers and additional members of the new GitLab Partner Program to help our customers provide better code to production more quickly.”
            author: William Rose
            job: Open Source team Manager at Carahsoft, which serves as GitLab’s master government distributor
            url: /customers/knowbe4/
          - quote: |
              “Being a GitLab partner is a huge advantage for Force 3. They enable our team with a robust and self-facilitating partner program to invest ahead of the curve with an approach of positioning a single application to manage the full software development lifecycle for mission critical applications in the Federal Government. This partnership allows us to provide our customers with next generation capabilities for our customized cloud migration services and enterprise software solutions business practices.”
            author: Marty Calambro
            job: Vice President of sales at Force 3
          - quote: |
              “HashiCorp has been working closely with GitLab to enhance the experience for our joint customers, and we are looking forward to even greater connections between our companies and products as part of this new program. Our joint efforts with GitLab have been very positive, and the HashiCorp Terraform and Vault product integrations offer a great out-of-the-box experience to customers looking to secure and manage their infrastructure as part of their DevOps lifecycle." 
            author:  Burzin Patel
            job: Vice President, global alliances at HashiCorp
            url: /customers/siemens/
          - quote: |
              "GitLab provides a solution to many of the common delivery issues faced across our customer base. The new channel program reinforces IntegrationQA's service led DevOps methodology and we look forward to strengthening our partnership with Gitlab. Together, the IntegrationQA and GitLab teams bring strong experience architecting, hardening and integrating GitLab to meet the rigor and delivery demands of Federal, State and Local governments as well as large corporate enterprises.”
            author: Chris Wellington
            job: Managing Director at IntegrationQA
            url: /customers/siemens/
          - quote: |
              “GitLab is leading the evolution of DevOps by reducing software development phases based on the cloud through a single tool, which optimizes work efficiency and easily integrates existing tools. Megazone will pioneer the market to promote DevOps and cloud native computing, hand in hand with GitLab.”
            author:  Joowan Lee
            job: CEO at Megazone Cloud
            url: /customers/siemens/
          - quote: |
              "2nd Watch is excited to partner with GitLab, especially with the benefits of the new GitLab Partner Program. We recently launched a 2nd Watch Managed DevOps service offering and look forward to leveraging GitLab's partner program benefits including sales and technical certifications along with go-to-market joint initiatives.”
            author:  Stefana Muller
            job: Senior Product Manager at 2nd Watch
            url: /customers/siemens/                                               
